import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:resla_admin_v_1_0/app/modules/home/controllers/home_controller.dart';

class InventoryController extends GetxController {
  //TODO: Implement InventoryController
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  TextEditingController addtoitemnamecontroller = TextEditingController();
  TextEditingController addtoitemstockontroller = TextEditingController();
  TextEditingController edittoitemnamecontroller = TextEditingController();
  TextEditingController edittoitemstockontroller = TextEditingController();
  TextEditingController addtostock = TextEditingController();
  TextEditingController searchController = TextEditingController();
  RxString selecteddropdown = ''.obs;
  RxString searchfilter = ''.obs;

  String restoid;
  void addtoItem(BuildContext context) async {
    try {
      Random random = new Random();
      int randomNumber = random.nextInt(1000000);
      await firestore
          .collection('restaurants')
          .doc(restoid)
          .collection('inventory')
          .doc('$randomNumber')
          .set({
        "id": randomNumber,
        "name": addtoitemnamecontroller.text,
        "stockamount": int.parse(addtoitemstockontroller.text),
      });
    } on PlatformException catch (e) {
      Get.snackbar("Error", e.toString());
    } on Exception catch (e) {
      Get.snackbar("Error", e.toString());
    }
    addtoitemnamecontroller.clear();
    addtoitemstockontroller.clear();
    Navigator.pop(context);
  }

  void updatetoItem(BuildContext context, String id) async {
    try {
      await firestore
          .collection('restaurants')
          .doc(restoid)
          .collection('inventory')
          .doc(id)
          .update({
        "name": edittoitemnamecontroller.text,
        "stockamount": int.parse(edittoitemstockontroller.text)
      });
    } catch (e) {
      Get.snackbar("Error", e.toString());
    }
    edittoitemnamecontroller.clear();
    edittoitemstockontroller.clear();
    Navigator.pop(context);
  }

  void additemstocks(BuildContext context, String id, int preval) async {
    try {
      await firestore
          .collection('restaurants')
          .doc(restoid)
          .collection('inventory')
          .doc(id)
          .update({"stockamount": (int.parse(addtostock.text) + preval)});
    } catch (e) {
      Get.snackbar("Error", e.toString());
    }

    addtostock.clear();
    Navigator.pop(context);
  }

  @override
  void onInit() {
    restoid = Get.find<HomeController>().restoid;
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
