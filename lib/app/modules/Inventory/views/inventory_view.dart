import 'package:flutter/material.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../controllers/inventory_controller.dart';

class InventoryView extends GetView<InventoryController> {
  showAddToItem(BuildContext context) {
    showAnimatedDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Add an inventory item",
                style: GoogleFonts.acme(),
              ),
              Divider(),
            ],
          ),
          content: Container(
            height: 150,
            width: 300,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: controller.addtoitemnamecontroller,
                  decoration: InputDecoration(
                    labelText: "Item Name",
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey.shade600, width: 2.0)),
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                  ),
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Colors.grey[700]),
                ),
                SizedBox(
                  height: 25,
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: Colors.red)),
                  child: TextField(
                    controller: controller.addtoitemstockontroller,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: "Stock Quantity",
                      labelStyle: GoogleFonts.aBeeZee(),
                    ),
                  ),
                ),
              ],
            ),
          ),
          actions: [
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text("Add"),
              onPressed: () {
                if (controller.addtoitemnamecontroller.text != null &&
                    controller.addtoitemstockontroller.text != null) {
                  controller.addtoItem(context);
                }
              },
            ),
          ],
        );
      },
      animationType: DialogTransitionType.slideFromRight,
      curve: Curves.fastOutSlowIn,
      duration: Duration(seconds: 1),
    );
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Positioned(
        left: width * 0.18,
        child: AnimatedContainer(
          duration: Duration(milliseconds: 375),
          height: MediaQuery.of(context).size.height,
          width: width / 1.22,
          color: Color(0xffF1F6FA),
          child: Scaffold(
              backgroundColor: Color(0xffF1F6FA),
              body: Container(
                padding: EdgeInsets.all(40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Inventory Management",
                      style: GoogleFonts.hammersmithOne(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                      ),
                    ),
                    Divider(),
                    Container(
                      width: width / 1.3,
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                            child: Text(
                              'All Items',
                              style: GoogleFonts.lato(
                                  fontWeight: FontWeight.w600,
                                  fontSize:
                                      MediaQuery.of(context).size.height / 35),
                            ),
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          LinearPercentIndicator(
                            lineHeight: 3.0,
                            percent: 0.2,
                            backgroundColor: Colors.grey[300],
                            progressColor: Colors.orange,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Obx(
                                      () => Container(
                                        height: 35,
                                        width: 150,
                                        decoration: BoxDecoration(
                                            color: Colors.grey[200],
                                            borderRadius:
                                                BorderRadius.circular(2)),
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 0),
                                          child: DropdownButtonHideUnderline(
                                            child: DropdownButton<String>(
                                              hint: Text(
                                                "  All",
                                                style: GoogleFonts.lato(),
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                              value: controller.selecteddropdown
                                                          .value.length ==
                                                      0
                                                  ? "All"
                                                  : controller
                                                      .selecteddropdown.value,
                                              items: [
                                                "All",
                                                "Running out",
                                                "Out of stock",
                                                "In Stock"
                                              ].map((String value) {
                                                return new DropdownMenuItem<
                                                    String>(
                                                  value: value,
                                                  child: new Text(value),
                                                );
                                              }).toList(),
                                              onChanged: (_) {
                                                controller.searchfilter.value =
                                                    '';
                                                controller.searchController
                                                    .clear();
                                                controller
                                                    .selecteddropdown.value = _;
                                              },
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                        height: 35,
                                        width: 150,
                                        decoration: BoxDecoration(
                                            color: Colors.grey[200],
                                            borderRadius:
                                                BorderRadius.circular(2)),
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 13),
                                          child: TextField(
                                            controller:
                                                controller.searchController,
                                            decoration: InputDecoration(
                                                border: InputBorder.none),
                                          ),
                                        )),
                                    GestureDetector(
                                      onTap: () {
                                        controller.searchfilter.value =
                                            controller.searchController.text;
                                      },
                                      child: Container(
                                        height: 35,
                                        width: 35,
                                        decoration: BoxDecoration(
                                          color: Colors.orange,
                                          borderRadius:
                                              BorderRadius.circular(2),
                                        ),
                                        child: Center(
                                            child: Icon(
                                          Icons.search,
                                          color: Colors.white,
                                        )),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 0),
                                  child: ElevatedButton(
                                      style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.orange),
                                          elevation:
                                              MaterialStateProperty.all<double>(
                                                  10)),
                                      onPressed: () {
                                        showAddToItem(context);
                                      },
                                      child: Container(
                                        height: 35,
                                        child: Row(children: [
                                          Icon(
                                            Icons.add_circle_rounded,
                                            size: 18,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text('Add Item')
                                        ]),
                                      )),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                            child: Divider(
                              thickness: 2,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                            child: Obx(
                              () => StreamBuilder(
                                  stream: controller
                                              .searchfilter.value.length ==
                                          0
                                      ? controller.selecteddropdown.value
                                                  .length ==
                                              0
                                          ? controller.firestore
                                              .collection('restaurants')
                                              .doc(controller.restoid)
                                              .collection('inventory')
                                              .snapshots()
                                          : controller.selecteddropdown.value ==
                                                  "Out of stock"
                                              ? controller.firestore
                                                  .collection('restaurants')
                                                  .doc(controller.restoid)
                                                  .collection('inventory')
                                                  .where("stockamount",
                                                      isEqualTo: 0)
                                                  .snapshots()
                                              : controller.selecteddropdown
                                                          .value ==
                                                      "In Stock"
                                                  ? controller.firestore
                                                      .collection('restaurants')
                                                      .doc(controller.restoid)
                                                      .collection('inventory')
                                                      .where("stockamount",
                                                          isGreaterThan: 10)
                                                      .snapshots()
                                                  : controller.selecteddropdown
                                                              .value ==
                                                          "Running out"
                                                      ? controller.firestore
                                                          .collection(
                                                              'restaurants')
                                                          .doc(controller
                                                              .restoid)
                                                          .collection(
                                                              'inventory')
                                                          .where("stockamount",
                                                              isLessThanOrEqualTo:
                                                                  10)
                                                          .snapshots()
                                                      : controller.firestore
                                                          .collection(
                                                              'restaurants')
                                                          .doc(controller
                                                              .restoid)
                                                          .collection(
                                                              'inventory')
                                                          .snapshots()
                                      : controller.firestore
                                          .collection('restaurants')
                                          .doc(controller.restoid)
                                          .collection('inventory')
                                          .where("name",
                                              isGreaterThanOrEqualTo:
                                                  controller.searchfilter.value)
                                          .snapshots(),
                                  builder: (BuildContext context,
                                      AsyncSnapshot snapshot) {
                                    return Container(
                                      child: PaginatedDataTable(
                                        dataRowHeight:
                                            MediaQuery.of(context).size.height /
                                                10.5,

                                        columns: <DataColumn>[
                                          DataColumn(
                                              label: Text('SN',
                                                  style: GoogleFonts.lato(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              tooltip:
                                                  'represents serial number of the items'),
                                          DataColumn(
                                              label: Text('Item Name',
                                                  style: GoogleFonts.lato(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              tooltip:
                                                  'represents the name of the Item'),
                                          DataColumn(
                                              label: Text('Total In stocks',
                                                  style: GoogleFonts.lato(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              tooltip:
                                                  'represents number of available stocks'),
                                          DataColumn(
                                              label: Text('Status',
                                                  style: GoogleFonts.lato(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              tooltip:
                                                  'represents status of the item'),
                                          DataColumn(
                                              label: Text('Actions',
                                                  style: GoogleFonts.lato(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              tooltip:
                                                  'represents the actions to be performed'),
                                          DataColumn(
                                              label: Text('Edit',
                                                  style: GoogleFonts.lato(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              tooltip:
                                                  'edit your inventory item'),
                                        ],
                                        source: snapshot.hasData
                                            ? DTSA(
                                                context: context,
                                                data: snapshot.data.docs,
                                                controller: controller)
                                            : DTSA(
                                                context: context,
                                                data: [],
                                                controller: controller),
                                        //Set Value for rowsPerPage based on comparing the actual data length with the PaginatedDataTable.defaultRowsPerPage
                                        rowsPerPage: 5,
                                      ),
                                    );
                                  }),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )),
        ));
  }
}

class DTSA extends DataTableSource {
  BuildContext context;
  DTSA({this.context, @required this.data, @required this.controller});
  var data;
  InventoryController controller;
  showEditItem(BuildContext context, String id) {
    showAnimatedDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Add an inventory item",
                style: GoogleFonts.acme(),
              ),
              Divider(),
            ],
          ),
          content: Container(
            height: 150,
            width: 300,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: controller.edittoitemnamecontroller,
                  decoration: InputDecoration(
                    labelText: "Item Name",
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey.shade600, width: 2.0)),
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                  ),
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Colors.grey[700]),
                ),
                SizedBox(
                  height: 25,
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: Colors.red)),
                  child: TextField(
                    controller: controller.edittoitemstockontroller,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: "Stock Quantity",
                      labelStyle: GoogleFonts.aBeeZee(),
                    ),
                  ),
                ),
              ],
            ),
          ),
          actions: [
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text("Update"),
              onPressed: () {
                if (controller.edittoitemnamecontroller.text != null &&
                    controller.edittoitemstockontroller.text != null) {
                  controller.updatetoItem(context, id);
                }
              },
            ),
          ],
        );
      },
      animationType: DialogTransitionType.slideFromRight,
      curve: Curves.fastOutSlowIn,
      duration: Duration(seconds: 1),
    );
  }

  showAddToStock(BuildContext context, String uid, int val) {
    showAnimatedDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Add to stock",
                style: GoogleFonts.acme(),
              ),
              Divider(),
            ],
          ),
          content: Container(
            height: 150,
            width: 300,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  ' Add to stock only after the stock arrives!',
                  style: GoogleFonts.abel(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 25,
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: Colors.red)),
                  child: TextField(
                    controller: controller.addtostock,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: "Total quantity",
                      labelStyle: GoogleFonts.aBeeZee(),
                    ),
                    keyboardType: TextInputType.number,
                  ),
                ),
              ],
            ),
          ),
          actions: [
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text("Add"),
              onPressed: () {
                if (controller.addtostock.text != null) {
                  controller.additemstocks(context, uid, val);
                } else {
                  Get.snackbar("Error", "Please make sure field is not empty");
                }
              },
            ),
          ],
        );
      },
      animationType: DialogTransitionType.slideFromRight,
      curve: Curves.fastOutSlowIn,
      duration: Duration(seconds: 1),
    );
  }

  @override
  DataRow getRow(int index) {
    return data.length != 0
        ? DataRow.byIndex(
            index: index,
            cells: [
              DataCell(Text(
                "${index + 1}",
                style: GoogleFonts.aBeeZee(),
              )),
              DataCell(Text(
                data[index]['name'],
                style: GoogleFonts.aBeeZee(),
              )),
              DataCell(Text(
                data[index]['stockamount'].toString(),
                style: GoogleFonts.aBeeZee(),
              )),
              DataCell(data[index]['stockamount'] == 0
                  ? Text(
                      'Out of Stock',
                      style: TextStyle(
                          color: Colors.red, fontWeight: FontWeight.bold),
                    )
                  : data[index]['stockamount'] > 10
                      ? Text(
                          'In stock',
                          style: TextStyle(
                              color: Colors.green, fontWeight: FontWeight.bold),
                        )
                      : Text(
                          'Running Out of Stock',
                          style: TextStyle(
                              color: Colors.purpleAccent,
                              fontWeight: FontWeight.bold),
                        )),
              DataCell(ElevatedButton(
                onPressed: () {
                  showAddToStock(
                      context, data[index].id, data[index]['stockamount']);
                },
                style: ButtonStyle(
                    padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                        EdgeInsets.all(0))),
                child: Container(
                  height: 30,
                  width: 120,
                  child: Center(
                      child: Text(
                    "Add to Stock",
                    style: GoogleFonts.aBeeZee(),
                  )),
                ),
              )),
              DataCell(
                IconButton(
                    icon: Icon(
                      Icons.edit,
                      size: 17,
                    ),
                    onPressed: () {
                      controller.edittoitemnamecontroller.text =
                          data[index]['name'];
                      controller.edittoitemstockontroller.text =
                          data[index]['stockamount'].toString();
                      showEditItem(context, data[index].id);
                    }),
              )
            ],
          )
        : DataRow.byIndex(
            index: index,
            cells: [
              DataCell(Text(
                '-',
                style: GoogleFonts.aBeeZee(),
              )),
              DataCell(Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 100,
                  width: 100,
                  child: Center(child: Text('-')),
                ),
              )),
              DataCell(Text(
                '-',
                style: GoogleFonts.aBeeZee(),
              )),
              DataCell(Text(
                "-",
                style: GoogleFonts.aBeeZee(),
              )),
              DataCell(Text(
                '-',
                style:
                    TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
              )),
              DataCell(ElevatedButton(
                onPressed: () {},
                style: ButtonStyle(
                    padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                        EdgeInsets.all(0))),
                child: Container(
                  height: 30,
                  width: 120,
                  child: Center(
                      child: Text(
                    "-",
                    style: GoogleFonts.aBeeZee(),
                  )),
                ),
              )),
            ],
          );
  }

  @override
  int get rowCount =>
      data.length; // Manipulate this to which ever value you wish

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => 0;
}
