import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jumping_dot/jumping_dot.dart';
import 'package:path/path.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:switcher_button/switcher_button.dart';

import '../controllers/menu_controller.dart';

class MenuView extends GetView<MenuController> {
  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = Obx(() => FlatButton(
          child: controller.stockinformationvisible.value
              ? Text("Back")
              : Text('Cancel'),
          onPressed: !controller.stockinformationvisible.value
              ? () {
                  if (controller.imageuploadindicator.value == false) {
                    controller.addmenunamecontroller.value.clear();
                    controller.addpricecontroller.value.clear();
                    controller.localimage.value = '';
                    Navigator.pop(context);
                  }
                }
              : () {
                  controller.stockinformationvisible.value = false;
                  controller.isfoucsactive.value = false;
                  controller.animationController.reset();
                },
        ));
    Widget continueButton = Obx(() => FlatButton(
        child: controller.stockinformationvisible.value
            ? Text("Save")
            : Text('Next'),
        onPressed: !controller.stockinformationvisible.value
            ? () async {
                if (controller.addmenunamecontroller.value.text.length != 0 &&
                    controller.addpricecontroller.value.text.length != 0 &&
                    controller.localimage.value.length != 0) {
                  controller.stockinformationvisible.value = true;
                } else {
                  Get.snackbar("Error", "Fill all the fields");
                }
              }
            : () async {
                if (controller.menuitemsid.length > 0) {
                  controller.saveindicator.value = true;
                  await controller.uploadNewMenuToFirebase();
                  Navigator.pop(context);
                } else {
                  Get.snackbar("Error",
                      "Please add at least one inventory item as ingredient");
                }
              }));

    // set up the AlertDialog
    Widget alert = Obx(() => AlertDialog(
          title: !controller.stockinformationvisible.value
              ? Text(
                  "Add a new item",
                  style: GoogleFonts.acme(),
                )
              : Text(
                  "Stock Information",
                  style: GoogleFonts.acme(),
                ),
          content: !controller.stockinformationvisible.value
              ? Container(
                  height: 360,
                  width: 400,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 400,
                        child: Row(
                          children: [
                            Expanded(
                              child: TextField(
                                controller:
                                    controller.addmenunamecontroller.value,
                                decoration: InputDecoration(
                                  labelText: "Item Name",
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.grey.shade600,
                                          width: 2.0)),
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                ),
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.grey[700]),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: TextField(
                                controller: controller.addpricecontroller.value,
                                decoration: InputDecoration(
                                  labelText: "Item Price",
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.grey.shade600,
                                          width: 2.0)),
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                ),
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.grey[700]),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(children: [
                        Expanded(
                          child: Container(
                            height: 60,
                            padding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 0),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                    color: Colors.grey.shade600, width: 1)),
                            child: StreamBuilder(
                                stream: controller.firestore
                                    .collection('restaurants')
                                    .doc(controller.restoid)
                                    .snapshots(),
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  List<String> tags = [];
                                  if (snapshot.hasData) {
                                    snapshot.data['menutags'].forEach((e) {
                                      if (e != "All") {
                                        tags.add(e);
                                      }
                                    });
                                  }
                                  return Obx(
                                    () => DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        hint: Text("Select a category"),
                                        value: controller.selectedaddmenucat
                                                    .value.length ==
                                                0
                                            ? null
                                            : controller
                                                .selectedaddmenucat.value,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            color: Colors.grey[800],
                                            fontSize: 16),
                                        isExpanded: true,
                                        items: tags.map((String value) {
                                          return new DropdownMenuItem<String>(
                                            value: value,
                                            child: new Text(value),
                                          );
                                        }).toList(),
                                        onChanged: (_) {
                                          controller.selectedaddmenucat.value =
                                              _;
                                        },
                                      ),
                                    ),
                                  );
                                }),
                          ),
                        ),
                      ]),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Upload Image",
                        style: GoogleFonts.acme(),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Obx(
                        () => Row(
                          children: [
                            Container(
                              height: 100,
                              width: 100,
                              color: Colors.grey[300],
                              child: controller.localimage.value.length == 0
                                  ? Center(child: Icon(Icons.upload))
                                  : Image(
                                      image: NetworkImage(
                                          controller.localimage.value),
                                      fit: BoxFit.fill,
                                    ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            controller.localimage.value.length == 0
                                ? Material(
                                    elevation: 2,
                                    color: Colors.orange,
                                    child: InkWell(
                                      onTap: () async {
                                        await controller.uploadToStorage();
                                      },
                                      child: Container(
                                        height: 35,
                                        width: 100,
                                        child: Center(
                                            child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Select",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            controller.imageuploadindicator
                                                        .value ==
                                                    false
                                                ? SizedBox()
                                                : Container(
                                                    height: 15,
                                                    width: 15,
                                                    child:
                                                        CircularProgressIndicator(
                                                      strokeWidth: 4,
                                                    ),
                                                  )
                                          ],
                                        )),
                                      ),
                                    ),
                                  )
                                : Material(
                                    elevation: 2,
                                    color: Colors.red,
                                    child: InkWell(
                                      onTap: () async {
                                        await controller.removeImageFromDb();
                                      },
                                      child: Container(
                                        height: 35,
                                        width: 100,
                                        child: Center(
                                            child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Remove",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            controller.imageuploadindicator
                                                        .value ==
                                                    false
                                                ? SizedBox()
                                                : Container(
                                                    height: 15,
                                                    width: 15,
                                                    child:
                                                        CircularProgressIndicator(
                                                      strokeWidth: 3,
                                                    ),
                                                  )
                                          ],
                                        )),
                                      ),
                                    ),
                                  ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              : Container(
                  height: 360,
                  width: 400,
                  child: Column(
                    children: [
                      Card(
                        elevation: 5,
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(7)),
                        child: AnimatedBuilder(
                          animation: controller.animationController,
                          builder: (BuildContext context, Widget child) {
                            return Obx(
                              () => Container(
                                height: controller.isfoucsactive.value
                                    ? (50 + (controller.animation.value * 120))
                                    : 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(7),
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      height: 50,
                                      width: 400,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 20, vertical: 0),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(7),
                                          color: Colors.grey[300]),
                                      child: FocusScope(
                                        child: Focus(
                                          onFocusChange: (value) {
                                            controller.isfoucsactive.value =
                                                value;

                                            controller.animationController
                                                .forward();
                                          },
                                          child: TextField(
                                            controller: controller
                                                .searchinventorycontroller
                                                .value,
                                            textAlign: TextAlign.start,
                                            onChanged: (e) {
                                              print(controller.myquery);
                                              print(
                                                  controller.searchquery.value);
                                              final querydata = controller
                                                  .myquery
                                                  .where((p0) {
                                                final topiclower =
                                                    p0.toLowerCase();
                                                final selectedlower =
                                                    e.toLowerCase();
                                                return topiclower
                                                    .contains(selectedlower);
                                              }).toList();
                                              controller.searchquery.value =
                                                  querydata;
                                            },
                                            decoration: InputDecoration(
                                                border: InputBorder.none,
                                                alignLabelWithHint: true,
                                                hintText:
                                                    "Search item in inventory and add",
                                                hintStyle: TextStyle(
                                                    color: Colors.grey[500],
                                                    fontSize: 15),
                                                prefixIcon: Icon(Icons.search)),
                                          ),
                                        ),
                                      ),
                                    ),
                                    controller.isfoucsactive.value
                                        ? Container(
                                            height: controller.animation.value *
                                                120,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(7),
                                              color: Colors.white,
                                            ),
                                            child: StreamBuilder(
                                              stream: controller.firestore
                                                  .collection('restaurants')
                                                  .doc(controller.restoid)
                                                  .collection('inventory')
                                                  .snapshots(),
                                              builder: (BuildContext context,
                                                  AsyncSnapshot snapshot) {
                                                List data = [];
                                                List data2 = [];
                                                if (snapshot.hasData) {
                                                  snapshot.data.docs
                                                      .forEach((e) {
                                                    data.add(e['name']);
                                                    data2.add({
                                                      "name": e['name'],
                                                      "id": e['id'],
                                                      "stockamount":
                                                          e['stockamount']
                                                    });
                                                  });
                                                  controller.myquery = data;
                                                  controller.menuitemsid =
                                                      data2;
                                                }
                                                return Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 20,
                                                      vertical: 10),
                                                  child: Obx(
                                                    () {
                                                      print(controller
                                                          .searchquery
                                                          .value
                                                          .length);
                                                      return ListView.separated(
                                                          itemBuilder:
                                                              (context, index) {
                                                            print(controller
                                                                .searchquery
                                                                .value
                                                                .length);

                                                            return Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    Text(
                                                                      "${index + 1}. ",
                                                                      style: TextStyle(
                                                                          color: Colors.grey[
                                                                              800],
                                                                          fontSize:
                                                                              14),
                                                                    ),
                                                                    SizedBox(
                                                                      width: 20,
                                                                    ),
                                                                    Text(
                                                                      snapshot.hasData
                                                                          ? controller.searchquery.value.length == 0
                                                                              ? controller.myquery[index]
                                                                              : controller.searchquery.value[index]
                                                                          : '-',
                                                                      style: TextStyle(
                                                                          color: Colors.grey[
                                                                              800],
                                                                          fontSize:
                                                                              14),
                                                                    ),
                                                                  ],
                                                                ),
                                                                snapshot.hasData
                                                                    ? controller
                                                                            .addedInventories
                                                                            .value
                                                                            .contains(controller.searchquery.value.length == 0
                                                                                ? controller.myquery[index]
                                                                                : controller.searchquery.value[index])
                                                                        ? GestureDetector(
                                                                            onTap:
                                                                                () {
                                                                              if (controller.searchquery.value.length == 0) {
                                                                                List temdata = [];
                                                                                controller.addedInventories.value.forEach((element) {
                                                                                  if (element != controller.myquery[index]) {
                                                                                    temdata.add(element);
                                                                                  }
                                                                                });
                                                                                controller.addedInventories.value = temdata;
                                                                                controller.animationController.reverse().then((value) {
                                                                                  controller.isfoucsactive.value = false;
                                                                                  FocusScope.of(context).unfocus();
                                                                                  controller.animationController.stop();
                                                                                });
                                                                                controller.mycont.removeAt(index);
                                                                              } else {
                                                                                List temdata = [];
                                                                                controller.addedInventories.value.forEach((element) {
                                                                                  if (element != controller.searchquery[index]) {
                                                                                    temdata.add(element);
                                                                                  }
                                                                                });
                                                                                controller.addedInventories.value = temdata;
                                                                                controller.animationController.reverse().then((value) {
                                                                                  controller.isfoucsactive.value = false;
                                                                                  FocusScope.of(context).unfocus();
                                                                                  controller.animationController.stop();
                                                                                });
                                                                                controller.mycont.removeAt(index);
                                                                              }
                                                                            },
                                                                            child:
                                                                                Icon(Icons.close, size: 18),
                                                                          )
                                                                        : GestureDetector(
                                                                            onTap:
                                                                                () {
                                                                              if (controller.searchquery.value.length == 0) {
                                                                                controller.addedInventories.value = controller.addedInventories.value +
                                                                                    [
                                                                                      controller.myquery[index]
                                                                                    ];
                                                                                controller.mycont.add(TextEditingController());
                                                                              } else {
                                                                                controller.addedInventories.value = controller.addedInventories.value +
                                                                                    [
                                                                                      controller.searchquery.value[index]
                                                                                    ];
                                                                                controller.mycont.add(TextEditingController());
                                                                              }

                                                                              controller.animationController.reverse().then((value) {
                                                                                controller.isfoucsactive.value = false;
                                                                                FocusScope.of(context).unfocus();
                                                                                controller.animationController.stop();
                                                                              });
                                                                            },
                                                                            child:
                                                                                Icon(
                                                                              Icons.add,
                                                                              size: 18,
                                                                            ),
                                                                          )
                                                                    : SizedBox()
                                                              ],
                                                            );
                                                          },
                                                          separatorBuilder:
                                                              (c, i) {
                                                            return Divider();
                                                          },
                                                          itemCount: snapshot
                                                                  .hasData
                                                              ? controller
                                                                          .searchquery
                                                                          .value
                                                                          .length ==
                                                                      0
                                                                  ? 3
                                                                  : controller
                                                                              .searchquery
                                                                              .value
                                                                              .length >
                                                                          3
                                                                      ? 3
                                                                      : controller
                                                                          .searchquery
                                                                          .value
                                                                          .length
                                                              : 3);
                                                    },
                                                  ),
                                                );
                                              },
                                            ),
                                          )
                                        : SizedBox()
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                          child: Divider()),
                      Expanded(
                          child: Obx(
                        () => ListView.separated(
                            itemBuilder: (con, index) {
                              return Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Container(
                                    height: 55,
                                    width: 150,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        border: Border.all(
                                            color: Colors.grey.shade600,
                                            width: 2.0)),
                                    child: Center(
                                      child: Text(
                                        controller
                                            .addedInventories.value[index],
                                        style: TextStyle(
                                            color: Colors.grey[800],
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 55,
                                    width: 150,
                                    child: TextField(
                                      controller: controller.mycont[index],
                                      decoration: InputDecoration(
                                          labelText: "Quantity per order",
                                          border: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey.shade600,
                                                  width: 2.0)),
                                          floatingLabelBehavior:
                                              FloatingLabelBehavior.always,
                                          disabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey.shade600,
                                                  width: 2.0)),
                                          focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey.shade600,
                                                  width: 2.0))),
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.grey[700]),
                                    ),
                                  ),
                                ],
                              );
                            },
                            separatorBuilder: (con, index) {
                              return Divider();
                            },
                            itemCount:
                                controller.addedInventories.value.length),
                      ))
                    ],
                  ),
                ),
          actions: [
            cancelButton,
            continueButton,
          ],
        ));

    // show the dialog
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Positioned(
        left: width * 0.18,
        child: AnimatedContainer(
          duration: Duration(milliseconds: 375),
          height: MediaQuery.of(context).size.height,
          width: width,
          color: Color(0xffF1F6FA),
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(40.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Manage Products",
                    style: GoogleFonts.hammersmithOne(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    ),
                  ),
                  Divider(),
                  Container(
                    width: width / 1.3,
                    padding: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                          child: Text(
                            'Items',
                            style: GoogleFonts.lato(
                                fontWeight: FontWeight.w600,
                                fontSize:
                                    MediaQuery.of(context).size.height / 35),
                          ),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        LinearPercentIndicator(
                          lineHeight: 3.0,
                          percent: 0.2,
                          backgroundColor: Colors.grey[300],
                          progressColor: Colors.orange,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  ElevatedButton(
                                      style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.orange),
                                          elevation:
                                              MaterialStateProperty.all<double>(
                                                  10)),
                                      onPressed: () {
                                        showAlertDialog(context);
                                      },
                                      child: Container(
                                        height: 35,
                                        child: Row(children: [
                                          Icon(
                                            Icons.add_circle_rounded,
                                            size: 18,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text('Add Item')
                                        ]),
                                      )),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  ElevatedButton(
                                      style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.orange),
                                          elevation:
                                              MaterialStateProperty.all<double>(
                                                  10)),
                                      onPressed: () {
                                        Alert(
                                          context: context,
                                          title: "",
                                          style: AlertStyle(
                                              titleStyle:
                                                  TextStyle(fontSize: 1)),
                                          content: Stack(
                                            children: [
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    'Select to edit category or keep all for adding new',
                                                    style: GoogleFonts.acme(
                                                        fontSize: 15),
                                                  ),
                                                  SizedBox(
                                                    height: 15,
                                                  ),
                                                  Container(
                                                    height: 45,
                                                    width: 280,
                                                    decoration: BoxDecoration(
                                                        color: Colors.grey[200],
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(2)),
                                                    child: Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 5,
                                                          vertical: 0),
                                                      child: StreamBuilder(
                                                          stream: controller
                                                              .firestore
                                                              .collection(
                                                                  'restaurants')
                                                              .doc(controller
                                                                  .restoid)
                                                              .snapshots(),
                                                          builder: (BuildContext
                                                                  context,
                                                              AsyncSnapshot
                                                                  snapshot) {
                                                            List<String> tags =
                                                                [];
                                                            if (snapshot
                                                                .hasData) {
                                                              snapshot.data[
                                                                      'menutags']
                                                                  .forEach((e) {
                                                                tags.add(e);
                                                              });

                                                              return Obx(
                                                                () =>
                                                                    DropdownButtonHideUnderline(
                                                                  child:
                                                                      DropdownButton<
                                                                          String>(
                                                                    hint: Text(
                                                                      "   All",
                                                                      style: GoogleFonts
                                                                          .lato(),
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                    ),
                                                                    value: controller.selecteddropdown.value.length ==
                                                                            0
                                                                        ? "All"
                                                                        : controller
                                                                            .selecteddropdown
                                                                            .value,
                                                                    items: tags.map(
                                                                        (String
                                                                            value) {
                                                                      return new DropdownMenuItem<
                                                                          String>(
                                                                        value:
                                                                            value,
                                                                        child: new Text(
                                                                            value),
                                                                      );
                                                                    }).toList(),
                                                                    onChanged:
                                                                        (_) {
                                                                      if (_ !=
                                                                          "All") {
                                                                        controller
                                                                            .categorynameController
                                                                            .value
                                                                            .text = _;
                                                                        controller
                                                                            .selecteddropdown
                                                                            .value = _;
                                                                      } else {
                                                                        controller
                                                                            .categorynameController
                                                                            .value
                                                                            .clear();
                                                                        controller
                                                                            .selecteddropdown
                                                                            .value = "";
                                                                      }
                                                                    },
                                                                  ),
                                                                ),
                                                              );
                                                            } else {
                                                              return Text(
                                                                "  All",
                                                                style:
                                                                    GoogleFonts
                                                                        .lato(),
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                              );
                                                            }
                                                          }),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 40,
                                                  ),
                                                  Text(
                                                    'Enter the category name',
                                                    style: GoogleFonts.acme(
                                                        fontSize: 15),
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Obx(
                                                    () => TextField(
                                                      controller: controller
                                                          .categorynameController
                                                          .value,
                                                      decoration:
                                                          InputDecoration(
                                                              suffixIcon: controller
                                                                          .selecteddropdown
                                                                          .value !=
                                                                      ""
                                                                  ? IconButton(
                                                                      onPressed:
                                                                          () async {
                                                                        await controller
                                                                            .executeCategoryDelete();
                                                                        Navigator.pop(
                                                                            context);
                                                                      },
                                                                      icon:
                                                                          Icon(
                                                                        Icons
                                                                            .delete,
                                                                        color: Colors
                                                                            .red,
                                                                      ),
                                                                    )
                                                                  : null),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                          buttons: [
                                            DialogButton(
                                              child: Text(
                                                "Cancel",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 20),
                                              ),
                                              onPressed: () =>
                                                  Navigator.pop(context),
                                              color: Color.fromRGBO(
                                                  0, 179, 134, 1.0),
                                            ),
                                            DialogButton(
                                              child: Text(
                                                "Save",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 20),
                                              ),
                                              onPressed: () async {
                                                await controller
                                                    .onButtonPressed();
                                                Navigator.pop(context);
                                              },
                                              gradient: LinearGradient(colors: [
                                                Color.fromRGBO(
                                                    116, 116, 191, 1.0),
                                                Color.fromRGBO(
                                                    52, 138, 199, 1.0)
                                              ]),
                                            )
                                          ],
                                        ).show();
                                      },
                                      child: Container(
                                        height: 35,
                                        child: Row(children: [
                                          Icon(
                                            Icons.add_circle_rounded,
                                            size: 18,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text('Category Settings')
                                        ]),
                                      ))
                                ],
                              ),
                              Row(
                                children: [
                                  Container(
                                    height: 35,
                                    width: 150,
                                    decoration: BoxDecoration(
                                        color: Colors.grey[200],
                                        borderRadius: BorderRadius.circular(2)),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 5, vertical: 0),
                                      child: StreamBuilder(
                                          stream: controller.firestore
                                              .collection('restaurants')
                                              .doc(controller.restoid)
                                              .snapshots(),
                                          builder: (BuildContext context,
                                              AsyncSnapshot snapshot) {
                                            List<String> tags = [];
                                            if (snapshot.hasData) {
                                              snapshot.data['menutags']
                                                  .forEach((e) {
                                                tags.add(e);
                                              });
                                              return Obx(
                                                () =>
                                                    DropdownButtonHideUnderline(
                                                  child: DropdownButton<String>(
                                                    hint: Text(
                                                      "  All",
                                                      style: GoogleFonts.lato(),
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                    value: tags.contains(
                                                            controller
                                                                .selectedfilter
                                                                .value)
                                                        ? controller
                                                            .selectedfilter
                                                            .value
                                                        : null,
                                                    items: tags
                                                        .map((String value) {
                                                      return new DropdownMenuItem<
                                                          String>(
                                                        value: value,
                                                        child: new Text(value),
                                                      );
                                                    }).toList(),
                                                    onChanged: (_) {
                                                      controller.searchvalue
                                                          .value = '';
                                                      controller
                                                          .txtcontroller.value
                                                          .clear();
                                                      controller.selectedfilter
                                                          .value = _;
                                                    },
                                                  ),
                                                ),
                                              );
                                            } else {
                                              return Text(
                                                "  All",
                                                style: GoogleFonts.lato(),
                                                overflow: TextOverflow.ellipsis,
                                              );
                                            }
                                          }),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                      height: 35,
                                      width: 150,
                                      decoration: BoxDecoration(
                                          color: Colors.grey[200],
                                          borderRadius:
                                              BorderRadius.circular(2)),
                                      child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 10),
                                            child: TextField(
                                                controller: controller
                                                    .txtcontroller.value,
                                                decoration: InputDecoration(
                                                    border: InputBorder.none)),
                                          ))),
                                  Container(
                                    height: 35,
                                    width: 35,
                                    decoration: BoxDecoration(
                                      color: Colors.orange,
                                      borderRadius: BorderRadius.circular(2),
                                    ),
                                    child: Center(
                                        child: IconButton(
                                      onPressed: () {
                                        controller.searchvalue.value =
                                            controller.txtcontroller.value.text;
                                      },
                                      icon: Icon(
                                        Icons.search,
                                        color: Colors.white,
                                      ),
                                    )),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                          child: Divider(
                            thickness: 2,
                          ),
                        ),
                        Obx(
                          () => Padding(
                            padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                            child: StreamBuilder(
                                stream: controller.searchvalue.value.length == 0
                                    ? controller.selectedfilter == "All"
                                        ? controller.firestore
                                            .collection('restaurants')
                                            .doc(controller.restoid)
                                            .collection('menus')
                                            .snapshots()
                                        : controller.firestore
                                            .collection('restaurants')
                                            .doc(controller.restoid)
                                            .collection('menus')
                                            .where("tags",
                                                isEqualTo:
                                                    "${controller.selectedfilter.value}")
                                            .snapshots()
                                    : controller.firestore
                                        .collection('restaurants')
                                        .doc(controller.restoid)
                                        .collection('menus')
                                        .where("name",
                                            isGreaterThanOrEqualTo:
                                                "${controller.txtcontroller.value.text}")
                                        .snapshots(),
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  return Container(
                                    child: PaginatedDataTable(
                                      dataRowHeight:
                                          MediaQuery.of(context).size.height /
                                              10,

                                      columns: <DataColumn>[
                                        DataColumn(
                                            label: Text('SN',
                                                style: GoogleFonts.lato(
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            tooltip:
                                                'represents serial number of the orders'),
                                        DataColumn(
                                            label: Text('Item Image',
                                                style: GoogleFonts.lato(
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            tooltip:
                                                'represents the image of the item'),
                                        DataColumn(
                                            label: Text('Item Name',
                                                style: GoogleFonts.lato(
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            tooltip:
                                                'represents the name of the Item'),
                                        DataColumn(
                                            label: Text('Category Name',
                                                style: GoogleFonts.lato(
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            tooltip:
                                                'represents the name of the category'),
                                        DataColumn(
                                            label: Text('Last Edited',
                                                style: GoogleFonts.lato(
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            tooltip:
                                                'represents the edited date of the product'),
                                        DataColumn(
                                            label: Text('Available',
                                                style: GoogleFonts.lato(
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            tooltip:
                                                'represents the availability of the Item'),
                                        DataColumn(
                                            label: Text('Price',
                                                style: GoogleFonts.lato(
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            tooltip:
                                                'represents the total price of the item'),
                                        DataColumn(
                                            label: Text('Actions',
                                                style: GoogleFonts.lato(
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            tooltip:
                                                'represents the actions to be performed'),
                                      ],
                                      source: snapshot.hasData
                                          ? DTS(
                                              data: snapshot.data.docs,
                                              controller: controller,
                                              context: context)
                                          : DTS(
                                              data: [],
                                              controller: controller,
                                              context: context),
                                      //Set Value for rowsPerPage based on comparing the actual data length with the PaginatedDataTable.defaultRowsPerPage
                                      rowsPerPage: 4,
                                    ),
                                  );
                                }),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

class DTS extends DataTableSource {
  DTS({@required this.data, @required this.controller, @required this.context});
  MenuController controller;
  BuildContext context;
  var data;
  showEditAlertDialog(BuildContext context, String uid, String docid) {
    // set up the buttons
    Widget cancelButton = Obx(() => FlatButton(
          child: controller.editstockinformationvisible.value
              ? Text("Back")
              : Text('Cancel'),
          onPressed: !controller.editstockinformationvisible.value
              ? () {
                  if (controller.imageuploadindicator.value == false) {
                    controller.localimage.value = '';
                    controller.mycont.value = [];
                    controller.addedInventories.value = [];
                    controller.iseditfocusactive.value = false;
                    Navigator.pop(context);
                  }
                }
              : () {
                  controller.editstockinformationvisible.value = false;
                  controller.iseditfocusactive.value = false;
                  controller.animationController.reset();
                },
        ));
    Widget continueButton = Obx(() => FlatButton(
        child: controller.editstockinformationvisible.value
            ? Text("Save")
            : Text('Next'),
        onPressed: !controller.editstockinformationvisible.value
            ? () async {
                if (controller.editnamecontroller.value.text.length != 0 &&
                    controller.editpricecontroller.value.text.length != 0 &&
                    controller.localimage.value.length != 0) {
                  controller.editstockinformationvisible.value = true;
                } else {
                  Get.snackbar("Error", "Fill all the fields");
                }
              }
            : () async {
                controller.updateMenuToFirebase(context, docid);
              }));

    // Widget continueButton = FlatButton(
    //     child: Text("Save"),
    //     onPressed: () async {
    //       if (controller.editnamecontroller.value.text != null &&
    //           controller.editpricecontroller.value.text != null &&
    //           controller.localimage.value.length != 0) {
    //         FirebaseFirestore firestore = FirebaseFirestore.instance;
    //         await firestore
    //             .collection('restaurants')
    //             .doc(controller.restoid)
    //             .collection('menus')
    //             .doc(uid)
    //             .update({
    //           "image": controller.localimage.value,
    //           "lastedited": DateTime.now(),
    //           "name": controller.editnamecontroller.value.text,
    //           "price": controller.editpricecontroller.value.text,
    //           "stockquantity": 100,
    //           "tags": controller.selectededitcat.value
    //         });
    //         controller.localimage.value = "";
    //         Navigator.pop(context);
    //       } else {
    //         print('object');
    //       }
    //     });

    // set up the AlertDialog
    Widget alert = Obx(() => AlertDialog(
          title: !controller.editstockinformationvisible.value
              ? Text(
                  "Edit item",
                  style: GoogleFonts.acme(),
                )
              : Text(
                  "Stock Information",
                  style: GoogleFonts.acme(),
                ),
          content: !controller.editstockinformationvisible.value
              ? Container(
                  height: 360,
                  width: 400,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 400,
                        child: Row(
                          children: [
                            Expanded(
                              child: TextField(
                                controller: controller.editnamecontroller.value,
                                decoration: InputDecoration(
                                  labelText: "Item Name",
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.grey.shade600,
                                          width: 2.0)),
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                ),
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.grey[700]),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: TextField(
                                controller:
                                    controller.editpricecontroller.value,
                                decoration: InputDecoration(
                                  labelText: "Item Price",
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.grey.shade600,
                                          width: 2.0)),
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                ),
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.grey[700]),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(children: [
                        Expanded(
                          child: Container(
                            height: 60,
                            padding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 0),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                    color: Colors.grey.shade600, width: 1)),
                            child: StreamBuilder(
                                stream: controller.firestore
                                    .collection('restaurants')
                                    .doc(controller.restoid)
                                    .snapshots(),
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  List<String> tags = [];
                                  if (snapshot.hasData) {
                                    snapshot.data['menutags'].forEach((e) {
                                      if (e != "All") {
                                        tags.add(e);
                                      }
                                    });
                                  }
                                  return Obx(
                                    () => DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        hint: Text("Select a category"),
                                        value: tags.contains(controller
                                                .selectededitcat.value)
                                            ? controller.selectededitcat.value
                                            : null,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            color: Colors.grey[800],
                                            fontSize: 16),
                                        isExpanded: true,
                                        items: tags.map((String value) {
                                          return new DropdownMenuItem<String>(
                                            value: value,
                                            child: new Text(value),
                                          );
                                        }).toList(),
                                        onChanged: (_) {
                                          controller.selectededitcat.value = _;
                                        },
                                      ),
                                    ),
                                  );
                                }),
                          ),
                        ),
                      ]),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Upload Image",
                        style: GoogleFonts.acme(),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Obx(
                        () => Row(
                          children: [
                            Container(
                              height: 100,
                              width: 100,
                              color: Colors.grey[300],
                              child: controller.localimage.value.length == 0
                                  ? Center(child: Icon(Icons.upload))
                                  : Image(
                                      image: NetworkImage(
                                          controller.localimage.value),
                                      fit: BoxFit.fill,
                                    ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Material(
                              elevation: 2,
                              color: Colors.orange,
                              child: InkWell(
                                onTap: () async {
                                  controller.uploadToStorage();
                                },
                                child: Container(
                                  height: 35,
                                  width: 100,
                                  child: Center(
                                      child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Change",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      controller.imageuploadindicator.value ==
                                              false
                                          ? SizedBox()
                                          : Container(
                                              height: 15,
                                              width: 15,
                                              child: CircularProgressIndicator(
                                                strokeWidth: 4,
                                              ),
                                            )
                                    ],
                                  )),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              : Container(
                  height: 360,
                  width: 400,
                  child: Column(
                    children: [
                      Card(
                        elevation: 5,
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(7)),
                        child: AnimatedBuilder(
                          animation: controller.animationController,
                          builder: (BuildContext context, Widget child) {
                            return Obx(
                              () => Container(
                                height: controller.iseditfocusactive.value
                                    ? (50 + (controller.animation.value * 120))
                                    : 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(7),
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      height: 50,
                                      width: 400,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 20, vertical: 0),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(7),
                                          color: Colors.grey[300]),
                                      child: FocusScope(
                                        child: Focus(
                                          onFocusChange: (value) {
                                            controller.iseditfocusactive.value =
                                                value;

                                            controller.animationController
                                                .forward();
                                          },
                                          child: TextField(
                                            controller: controller
                                                .searchinventorycontroller
                                                .value,
                                            textAlign: TextAlign.start,
                                            onChanged: (e) {
                                              print(controller.myquery);
                                              print(
                                                  controller.searchquery.value);
                                              final querydata = controller
                                                  .myquery
                                                  .where((p0) {
                                                final topiclower =
                                                    p0.toLowerCase();
                                                final selectedlower =
                                                    e.toLowerCase();
                                                return topiclower
                                                    .contains(selectedlower);
                                              }).toList();
                                              controller.searchquery.value =
                                                  querydata;
                                            },
                                            decoration: InputDecoration(
                                                border: InputBorder.none,
                                                alignLabelWithHint: true,
                                                hintText:
                                                    "Search item in inventory and add",
                                                hintStyle: TextStyle(
                                                    color: Colors.grey[500],
                                                    fontSize: 15),
                                                prefixIcon: Icon(Icons.search)),
                                          ),
                                        ),
                                      ),
                                    ),
                                    controller.iseditfocusactive.value
                                        ? Container(
                                            height: controller.animation.value *
                                                120,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(7),
                                              color: Colors.white,
                                            ),
                                            child: StreamBuilder(
                                              stream: controller.firestore
                                                  .collection('restaurants')
                                                  .doc(controller.restoid)
                                                  .collection('inventory')
                                                  .snapshots(),
                                              builder: (BuildContext context,
                                                  AsyncSnapshot snapshot) {
                                                List data = [];
                                                List data2 = [];
                                                if (snapshot.hasData) {
                                                  snapshot.data.docs
                                                      .forEach((e) {
                                                    data.add(e['name']);
                                                    data2.add({
                                                      "name": e['name'],
                                                      "id": e['id'],
                                                      "stockamount":
                                                          e['stockamount']
                                                    });
                                                  });
                                                  controller.myquery = data;
                                                  controller.menuitemsid =
                                                      data2;
                                                }
                                                return Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 20,
                                                      vertical: 10),
                                                  child: Obx(
                                                    () {
                                                      print(controller
                                                          .searchquery
                                                          .value
                                                          .length);
                                                      return ListView.separated(
                                                          itemBuilder:
                                                              (context, index) {
                                                            print(controller
                                                                .searchquery
                                                                .value
                                                                .length);

                                                            return Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    Text(
                                                                      "${index + 1}. ",
                                                                      style: TextStyle(
                                                                          color: Colors.grey[
                                                                              800],
                                                                          fontSize:
                                                                              14),
                                                                    ),
                                                                    SizedBox(
                                                                      width: 20,
                                                                    ),
                                                                    Text(
                                                                      snapshot.hasData
                                                                          ? controller.searchquery.value.length == 0
                                                                              ? controller.myquery[index]
                                                                              : controller.searchquery.value[index]
                                                                          : '-',
                                                                      style: TextStyle(
                                                                          color: Colors.grey[
                                                                              800],
                                                                          fontSize:
                                                                              14),
                                                                    ),
                                                                  ],
                                                                ),
                                                                snapshot.hasData
                                                                    ? controller
                                                                            .addedInventories
                                                                            .value
                                                                            .contains(controller.searchquery.value.length == 0
                                                                                ? controller.myquery[index]
                                                                                : controller.searchquery.value[index])
                                                                        ? GestureDetector(
                                                                            onTap:
                                                                                () {
                                                                              if (controller.searchquery.value.length == 0) {
                                                                                List temdata = [];
                                                                                controller.addedInventories.value.forEach((element) {
                                                                                  if (element != controller.myquery[index]) {
                                                                                    temdata.add(element);
                                                                                  }
                                                                                });
                                                                                controller.addedInventories.value = temdata;
                                                                                controller.animationController.reverse().then((value) {
                                                                                  controller.isfoucsactive.value = false;
                                                                                  FocusScope.of(context).unfocus();
                                                                                  controller.animationController.stop();
                                                                                });
                                                                                controller.mycont.removeAt(index);
                                                                              } else {
                                                                                List temdata = [];
                                                                                controller.addedInventories.value.forEach((element) {
                                                                                  if (element != controller.searchquery[index]) {
                                                                                    temdata.add(element);
                                                                                  }
                                                                                });
                                                                                controller.addedInventories.value = temdata;
                                                                                controller.animationController.reverse().then((value) {
                                                                                  controller.isfoucsactive.value = false;
                                                                                  FocusScope.of(context).unfocus();
                                                                                  controller.animationController.stop();
                                                                                });
                                                                                controller.mycont.removeAt(index);
                                                                              }
                                                                            },
                                                                            child:
                                                                                Icon(Icons.close, size: 18),
                                                                          )
                                                                        : GestureDetector(
                                                                            onTap:
                                                                                () {
                                                                              if (controller.searchquery.value.length == 0) {
                                                                                controller.addedInventories.value = controller.addedInventories.value +
                                                                                    [
                                                                                      controller.myquery[index]
                                                                                    ];
                                                                                controller.mycont.add(TextEditingController());
                                                                              } else {
                                                                                controller.addedInventories.value = controller.addedInventories.value +
                                                                                    [
                                                                                      controller.searchquery.value[index]
                                                                                    ];
                                                                                controller.mycont.add(TextEditingController());
                                                                              }

                                                                              controller.animationController.reverse().then((value) {
                                                                                controller.isfoucsactive.value = false;
                                                                                FocusScope.of(context).unfocus();
                                                                                controller.animationController.stop();
                                                                              });
                                                                            },
                                                                            child:
                                                                                Icon(
                                                                              Icons.add,
                                                                              size: 18,
                                                                            ),
                                                                          )
                                                                    : SizedBox()
                                                              ],
                                                            );
                                                          },
                                                          separatorBuilder:
                                                              (c, i) {
                                                            return Divider();
                                                          },
                                                          itemCount: snapshot
                                                                  .hasData
                                                              ? controller
                                                                          .searchquery
                                                                          .value
                                                                          .length ==
                                                                      0
                                                                  ? 3
                                                                  : controller
                                                                              .searchquery
                                                                              .value
                                                                              .length >
                                                                          3
                                                                      ? 3
                                                                      : controller
                                                                          .searchquery
                                                                          .value
                                                                          .length
                                                              : 3);
                                                    },
                                                  ),
                                                );
                                              },
                                            ),
                                          )
                                        : SizedBox()
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                          child: Divider()),
                      Expanded(
                          child: Obx(
                        () => ListView.separated(
                            itemBuilder: (con, index) {
                              return Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Container(
                                    height: 55,
                                    width: 150,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        border: Border.all(
                                            color: Colors.grey.shade600,
                                            width: 2.0)),
                                    child: Center(
                                      child: Text(
                                        controller
                                            .addedInventories.value[index],
                                        style: TextStyle(
                                            color: Colors.grey[800],
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 55,
                                    width: 150,
                                    child: TextField(
                                      controller: controller.mycont[index],
                                      decoration: InputDecoration(
                                          labelText: "Quantity per order",
                                          border: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey.shade600,
                                                  width: 2.0)),
                                          floatingLabelBehavior:
                                              FloatingLabelBehavior.always,
                                          disabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey.shade600,
                                                  width: 2.0)),
                                          focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey.shade600,
                                                  width: 2.0))),
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.grey[700]),
                                    ),
                                  ),
                                ],
                              );
                            },
                            separatorBuilder: (con, index) {
                              return Divider();
                            },
                            itemCount:
                                controller.addedInventories.value.length),
                      ))
                    ],
                  ),
                ),
          actions: [
            cancelButton,
            continueButton,
          ],
        ));

    // show the dialog
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  DataRow getRow(int index) {
    if (data.length != 0) {
      List ingredientids = [];
      return DataRow.byIndex(
        index: index,
        cells: [
          DataCell(Text(
            "${index + 1}",
            style: GoogleFonts.aBeeZee(),
          )),
          DataCell(Padding(
            padding: const EdgeInsets.all(8.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.network(
                data[index]['image'],
                fit: BoxFit.fill,
                height: 150.0,
                width: 100.0,
              ),
            ),
          )),
          DataCell(Text(
            data[index]['name'],
            style: GoogleFonts.aBeeZee(),
          )),
          DataCell(Text(
            data[index]['tags'],
            style: GoogleFonts.aBeeZee(),
          )),
          DataCell(Text(
            '${data[index]['lastedited'].toDate().toString().split(':')[0]}:${data[index]['lastedited'].toDate().toString().split(':')[1]}',
            style: GoogleFonts.aBeeZee(),
          )),
          DataCell(
            data[index]['stockempty']
                ? FlutterSwitch(
                    width: 80.0,
                    height: 35.0,
                    valueFontSize: 14.0,
                    activeColor: Colors.green,
                    inactiveColor: Colors.red,
                    activeText: "Yes",
                    inactiveText: "No",
                    toggleSize: 20.0,
                    value: !data[index]['stockempty'],
                    borderRadius: 30.0,
                    padding: 10.0,
                    showOnOff: true,
                    onToggle: (val) async {
                      Get.snackbar("Error", "Ingredient stock is empty");
                    },
                  )
                : FlutterSwitch(
                    width: 80.0,
                    height: 35.0,
                    valueFontSize: 14.0,
                    activeColor: Colors.green,
                    inactiveColor: Colors.red,
                    activeText: "Yes",
                    inactiveText: "No",
                    toggleSize: 20.0,
                    value: data[index]['available'],
                    borderRadius: 30.0,
                    padding: 10.0,
                    showOnOff: true,
                    onToggle: (val) async {
                      await controller.firestore
                          .collection('restaurants')
                          .doc(controller.restoid)
                          .collection("menus")
                          .doc(data[index].id)
                          .update({"available": val});
                    },
                  ),
          ),
          DataCell(Text(
            data[index]['price'].toString(),
            style: GoogleFonts.aBeeZee(),
          )),
          DataCell(Row(
            children: [
              IconButton(
                  icon: Icon(
                    Icons.edit,
                    size: 17,
                  ),
                  onPressed: () async {
                    controller.editnamecontroller.value.text =
                        data[index]['name'];
                    controller.editpricecontroller.value.text =
                        data[index]['price'].toString();
                    controller.selectededitcat.value = data[index]['tags'];
                    controller.localimage.value = data[index]['image'];

                    QuerySnapshot mylistdata = await controller.firestore
                        .collection('restaurants')
                        .doc(controller.restoid)
                        .collection('inventory')
                        .get();
                    mylistdata.docs.forEach((e) {
                      data[index].data()['ingredientsid'].forEach((elm) {
                        if (e['id'] ==
                            int.parse(elm.toString().split(":")[0])) {
                          controller.addedInventories.add(e['name']);
                          controller.mycont.add(TextEditingController(
                              text: elm.toString().split(":")[1]));
                        }
                      });
                    });
                    showEditAlertDialog(
                        context, data[index].id, data[index].id);
                  }),
              IconButton(
                  icon: Icon(
                    Icons.delete,
                    color: Colors.red,
                    size: 17,
                  ),
                  onPressed: () async {
                    controller.deleteItem(data[index].id, context);
                  }),
            ],
          )),
        ],
      );
    } else {
      DataRow.byIndex(
        index: index,
        cells: [
          DataCell(Text(
            "-",
            style: GoogleFonts.aBeeZee(),
          )),
          DataCell(Text(
            "-",
            style: GoogleFonts.aBeeZee(),
          )),
          DataCell(Text(
            "-",
            style: GoogleFonts.aBeeZee(),
          )),
          DataCell(Text(
            "-",
            style: GoogleFonts.aBeeZee(),
          )),
          DataCell(Text(
            "-",
            style: GoogleFonts.aBeeZee(),
          )),
          DataCell(Text(
            "-",
            style: GoogleFonts.aBeeZee(),
          )),
          DataCell(Text(
            "-",
            style: GoogleFonts.aBeeZee(),
          )),
          DataCell(Text(
            "-",
            style: GoogleFonts.aBeeZee(),
          )),
        ],
      );
    }
  }

  @override
  int get rowCount =>
      data.length; // Manipulate this to which ever value you wish

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => 0;
}
