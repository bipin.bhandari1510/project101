import 'dart:html';
import 'dart:math';
import 'dart:typed_data';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'package:path/path.dart';
import 'package:resla_admin_v_1_0/app/modules/home/controllers/home_controller.dart';

class MenuController extends GetxController with SingleGetTickerProviderMixin {
  //TODO: Implement MenuController

  /*
  TODO:
  Implement cloud functions for automatic stock empty
  Implement numbers only in add to stock
  */
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  final picker = ImagePicker();
  RxList<dynamic> mycont = [].obs;
  FirebaseStorage _storage;
  RxBool availableonbasicofstock = true.obs;
  RxInt switchercheckcount = 1.obs;
  List menuitems = [];
  List myquery = [];
  List menuitemsid = [];
  RxList<dynamic> searchquery = [].obs;
  AnimationController animationController;
  Animation animation;
  RxBool imageuploadindicator = false.obs;
  Rx<TextEditingController> searchinventorycontroller =
      TextEditingController().obs;
  var isfoucsactive = false.obs;
  var iseditfocusactive = false.obs;
  RxBool saveindicator = false.obs;
  String url;
  var stockinformationvisible = false.obs;
  var editstockinformationvisible = false.obs;
  RxString localimage = ''.obs;
  RxList<dynamic> addedInventories = [].obs;
  RxString selectededitcat = ''.obs;
  RxString editImage = ''.obs;
  Uint8List _image;
  RxString selectedfilter = "All".obs;
  RxString selectedaddmenucat = "".obs;
  Rx<TextEditingController> txtcontroller = TextEditingController().obs;
  Rx<TextEditingController> categorynameController =
      TextEditingController().obs;
  Rx<TextEditingController> addmenunamecontroller = TextEditingController().obs;
  Rx<TextEditingController> addpricecontroller = TextEditingController().obs;
  bool activeeditcategoryanimation = false;
  RxString selecteddropdown = "".obs;
  String restoid;
  Rx<TextEditingController> editnamecontroller = TextEditingController().obs;
  Rx<TextEditingController> editpricecontroller = TextEditingController().obs;
  RxString searchvalue = ''.obs;
  void onButtonPressed() async {
    if (categorynameController.value.text.length >= 1) {
      String selectedDropdown = selecteddropdown.value;
      DocumentSnapshot menuTagsValue =
          await firestore.collection('restaurants').doc(restoid).get();
      List mytaglist = [];
      menuTagsValue.get('menutags').forEach((e) {
        mytaglist.add(e.toLowerCase());
      });
      if (mytaglist.contains(categorynameController.value.text.toLowerCase())) {
      } else {
        selecteddropdown.value = "";

        if (mytaglist.contains(selectedDropdown.toLowerCase())) {
          await firestore.collection('restaurants').doc(restoid).update({
            "menutags": FieldValue.arrayRemove([selectedDropdown])
          });
        }
        await firestore.collection('restaurants').doc(restoid).set({
          "menutags": FieldValue.arrayUnion([categorynameController.value.text])
        }, SetOptions(merge: true));
        categorynameController.value.clear();
      }
    }
  }

  changeStorage() async {
    InputElement input = FileUploadInputElement()..accept = 'image/*';
    FirebaseStorage fs = FirebaseStorage.instance;
    input.click();
    input.onChange.listen((event) {
      final file = input.files.first;
      final reader = FileReader();
      reader.readAsDataUrl(file);
      print(file.relativePath);
      reader.onLoadEnd.listen((event) async {
        imageuploadindicator.value = true;
        Random random = new Random();
        int randomNumber = random.nextInt(1000000);
        var snapshot = await fs.refFromURL(editImage.value).putBlob(file);
        String downloadUrl = await snapshot.ref.getDownloadURL();
        print(event.loaded);
        url = downloadUrl;
        localimage.value = downloadUrl;
        imageuploadindicator.value = false;
      });
    });
  }

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("   Deleting...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  deleteItem(String uid, BuildContext context) async {
    FirebaseStorage fs = FirebaseStorage.instance;
    showLoaderDialog(context);
    await firestore
        .collection('restaurants')
        .doc(restoid)
        .collection('menus')
        .doc(uid)
        .delete();
    Navigator.pop(context);
  }

  uploadToStorage() async {
    InputElement input = FileUploadInputElement()..accept = 'image/*';
    FirebaseStorage fs = FirebaseStorage.instance;
    input.click();
    input.onChange.listen((event) {
      final file = input.files.first;
      final reader = FileReader();
      reader.readAsDataUrl(file);
      print(file.relativePath);
      reader.onLoadEnd.listen((event) async {
        imageuploadindicator.value = true;
        Random random = new Random();
        int randomNumber = random.nextInt(1000000);
        var snapshot =
            await fs.ref().child('my_image/$randomNumber').putBlob(file);
        String downloadUrl = await snapshot.ref.getDownloadURL();
        print(event.loaded);
        url = downloadUrl;
        localimage.value = downloadUrl;
        imageuploadindicator.value = false;
      });
    });
//     FilePickerResult result =
//         await FilePicker.platform.pickFiles(type: FileType.image);

//     if (result != null) {
//       imageuploadindicator.value = true;
//       Uint8List fileBytes = result.files.first.bytes;
//       String fileName = result.files.first.name;
//       Random random = new Random();
//       int randomNumber = random.nextInt(1000000);
//       FirebaseStorage fs = FirebaseStorage.instance;
//       var snapshot = await fs
//           .ref()
//           .child('uploads/$randomNumber/$fileName')
//           .putData(_image);
//       String downloadUrl = await snapshot.ref.getDownloadURL();
//       url = downloadUrl;
//       localimage.value = downloadUrl;
//       imageuploadindicator.value = false;
//       imageuploadsuccess.value = true;
//       // Upload file
// //  await FirebaseStorage.instance.ref('uploads/$fileName').putData(fileBytes);
//     }
//     // Image fromPicker =
//     //     await ImagePickerWeb.getImage(outputType: ImageType.file);
//     // final picker = ImagePicker();
//     // final pickedFile = await picker.getImage(source: ImageSource.gallery);
//     // var _image = await pickedFile.;
//   }
  }

  void removeImageFromDb() async {
    FirebaseStorage fs = FirebaseStorage.instance;
    Reference ref = await fs.refFromURL(localimage.value);
    await ref.delete();
    localimage.value = '';
  }

  uploadNewMenuToFirebase() async {
    Random random = new Random();
    int randomNumber = random.nextInt(1000000);
    FirebaseStorage fs = FirebaseStorage.instance;
    bool isstockavailable = false;
    List menuids = [];
    for (int i = 0; i < addedInventories.value.length; i++) {
      menuitemsid.forEach((elm) {
        if (addedInventories.value[i] == elm['name']) {
          menuids.add("${elm['id']}:${mycont.value[i].text}");
          if (elm['stockamount'] <= 0) {
            isstockavailable = true;
          }
        }
      });
    }
    print(menuitemsid);
    await firestore
        .collection('restaurants')
        .doc(restoid)
        .collection('menus')
        .doc()
        .set({
      "id": randomNumber,
      "image": url,
      "lastedited": DateTime.now(),
      "name": addmenunamecontroller.value.text,
      "price": addpricecontroller.value.text,
      "stockquantity": 100,
      "tags": selectedaddmenucat.value,
      "ingredientsid": menuids,
      "available": true,
      "stockempty": isstockavailable,
    });
    addmenunamecontroller.value.clear();
    addpricecontroller.value.clear();
    addedInventories.value = [];
    mycont.value = [];
    saveindicator.value = false;
  }

  updateMenuToFirebase(BuildContext contxt, String docid) async {
    List menuids = [];
    print(addedInventories);
    bool isstockavailable = false;
    for (int i = 0; i < addedInventories.value.length; i++) {
      menuitemsid.forEach((elm) {
        if (addedInventories.value[i] == elm['name']) {
          menuids.add("${elm['id']}:${mycont.value[i].text}");
          if (elm['stockamount'] <= 0) {
            isstockavailable = true;
          }
        }
      });
    }
    await firestore
        .collection('restaurants')
        .doc(restoid)
        .collection('menus')
        .doc(docid)
        .update({
      "image": localimage.value,
      "lastedited": DateTime.now(),
      "name": editnamecontroller.value.text,
      "price": editpricecontroller.value.text,
      "stockquantity": 100,
      "tags": selectededitcat.value,
      "ingredientsid": menuids,
      "stockempty": isstockavailable
    });
    addedInventories.value = [];
    mycont.value = [];
    editnamecontroller.value.clear();
    editpricecontroller.value.clear();
    saveindicator.value = false;
    editstockinformationvisible.value = false;
    Navigator.pop(contxt);
  }

  void executeCategoryDelete() async {
    selecteddropdown.value = "";
    await firestore.collection('restaurants').doc(restoid).update({
      "menutags": FieldValue.arrayRemove([categorynameController.value.text])
    });
    categorynameController.value.clear();
  }

  @override
  void onInit() {
    categorynameController.value.text = "";
    restoid = Get.find<HomeController>().restoid;
    animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    animation =
        CurvedAnimation(parent: animationController, curve: Curves.linear);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
