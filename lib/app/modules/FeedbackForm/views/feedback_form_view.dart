import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/feedback_form_controller.dart';

class FeedbackFormView extends GetView<FeedbackFormController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FeedbackFormView'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'FeedbackFormView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
