import 'package:get/get.dart';

class FeedbackFormController extends GetxController {
  //TODO: Implement FeedbackFormController

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
