import 'package:get/get.dart';

import '../controllers/feedback_form_controller.dart';

class FeedbackFormBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FeedbackFormController>(
      () => FeedbackFormController(),
    );
  }
}
