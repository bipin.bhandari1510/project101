import 'package:flutter/material.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:resla_admin_v_1_0/app/modules/home/views/home_view.dart';

import '../controllers/auth_page_controller.dart';

class AuthPageView extends GetView<AuthPageController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: controller.scaffoldKey,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: LayoutBuilder(builder: (context, constraints) {
          print(constraints.maxWidth);
          if (constraints.minWidth <= 980.0 && constraints.minWidth >= 0.0) {
            return SafeArea(
                child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Color(0xfff12711), Color(0xfff5af19)],
                ),
              ),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(40),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.orange.withOpacity(0.8),
                          offset: Offset(-6.0, -6.0),
                          blurRadius: 30.0,
                        ),
                        BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          offset: Offset(6.0, 6.0),
                          blurRadius: 16.0,
                        ),
                      ],
                      color: Colors.white,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(40.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            " Name of the resturant:",
                            style: GoogleFonts.acme(
                                fontSize:
                                    MediaQuery.of(context).size.height / 50),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          FutureBuilder(
                            future: controller.firestore
                                .collection('resturants')
                                .get(),
                            builder: (BuildContext context,
                                    AsyncSnapshot snapshot) =>
                                Container(
                              //width: MediaQuery.of(context).size.width / 4,
                              height: 50,
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black26),
                                  borderRadius: BorderRadius.circular(12)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  hint: Text(
                                    "   Select your resturant",
                                    style: GoogleFonts.aBeeZee(),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  items: <String>['A', 'B', 'C', 'D']
                                      .map((String value) {
                                    return new DropdownMenuItem<String>(
                                      value: value,
                                      child: new Text(value),
                                    );
                                  }).toList(),
                                  onChanged: (_) {},
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            " Username",
                            style: GoogleFonts.acme(
                                fontSize:
                                    MediaQuery.of(context).size.height / 50),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 4,
                            child: TextFormField(
                              decoration: new InputDecoration(
                                labelText: "Enter Username",
                                labelStyle: GoogleFonts.aBeeZee(),
                                fillColor: Colors.white,
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(12.0),
                                  borderSide: new BorderSide(),
                                ),
                                //fillColor: Colors.green
                              ),
                              validator: (val) {
                                if (val.isEmpty) {
                                  return "Email cannot be empty";
                                } else {
                                  return null;
                                }
                              },
                              keyboardType: TextInputType.emailAddress,
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            "Password",
                            style: GoogleFonts.acme(
                                fontSize:
                                    MediaQuery.of(context).size.height / 50),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 4,
                            child: TextFormField(
                              decoration: new InputDecoration(
                                labelText: "Enter Password",
                                fillColor: Colors.white,
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(12.0),
                                  borderSide: new BorderSide(),
                                ),
                                //fillColor: Colors.green
                              ),
                              validator: (val) {
                                if (val.length == 0) {
                                  return "Email cannot be empty";
                                } else {
                                  return null;
                                }
                              },
                              keyboardType: TextInputType.emailAddress,
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          ElevatedButton(
                            onPressed: () {},
                            child: Container(
                              height: 45,
                              width: MediaQuery.of(context).size.width / 4,
                              child: Center(child: Text('Sign in')),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ));
          } else {
            return SafeArea(
                child: AnimatedBuilder(
              animation: controller.animationController,
              builder: (BuildContext context, child) => Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: [
                        Expanded(
                            child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Image.asset(
                                'assets/logo1.png',
                                height:
                                    MediaQuery.of(context).size.height / 2.3,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(25),
                                child: Text(
                                  "Welcome to Resla admin portal. Resla is always determined to provide quality resturated automation service to both consumers and business owners. If you face any difficulty contact us at any time. Thanks for choosing Resla to improve your business.",
                                  style: GoogleFonts.hammersmithOne(
                                      fontSize:
                                          MediaQuery.of(context).size.height /
                                              40,
                                      height: 1.4),
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Divider(),
                              Text("Contact us on:",
                                  style: GoogleFonts.lato(fontSize: 18)),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SignInButton(
                                    Buttons.Facebook,
                                    mini: true,
                                    onPressed: () {},
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  SignInButton(
                                    Buttons.LinkedIn,
                                    mini: true,
                                    onPressed: () {},
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  SignInButton(
                                    Buttons.Twitter,
                                    mini: true,
                                    onPressed: () {},
                                  )
                                ],
                              ),
                            ],
                          ),
                        )),
                        Container(
                          //  height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width / 1.7,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: [Color(0xfff12711), Color(0xfff5af19)],
                            ),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width / 3.5,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.orange.withOpacity(0.8),
                                      offset: Offset(-6.0, -6.0),
                                      blurRadius: 30.0,
                                    ),
                                    BoxShadow(
                                      color: Colors.black.withOpacity(0.1),
                                      offset: Offset(6.0, 6.0),
                                      blurRadius: 16.0,
                                    ),
                                  ],
                                  color: Colors.white,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(40.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        " Name of the resturant:",
                                        style: GoogleFonts.acme(
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                50),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                4,
                                        height: 50,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.black26),
                                            borderRadius:
                                                BorderRadius.circular(12)),
                                        child: Obx(
                                          () => DropdownButtonHideUnderline(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: DropdownButton<String>(
                                                hint: Text(
                                                  "Select your resturant",
                                                  style: GoogleFonts.aBeeZee(),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                                items: controller
                                                        .isloading.value
                                                    ? <String>['']
                                                        .map((String value) {
                                                        return new DropdownMenuItem<
                                                            String>(
                                                          value: value,
                                                          child:
                                                              new Text(value),
                                                        );
                                                      }).toList()
                                                    : controller.data.docs
                                                        .map((value) {
                                                        return new DropdownMenuItem<
                                                            String>(
                                                          value: value['name'],
                                                          child: new Text(
                                                              value['name']),
                                                        );
                                                      }).toList(),
                                                onChanged: (_) {
                                                  controller
                                                      .selectedresto.value = _;
                                                },
                                                value: controller.selectedresto
                                                            .value.length >
                                                        1
                                                    ? controller
                                                        .selectedresto.value
                                                    : controller.test,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Text(
                                        " Email",
                                        style: GoogleFonts.acme(
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                50),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                4,
                                        child: TextFormField(
                                          controller:
                                              controller.username_controller,
                                          decoration: new InputDecoration(
                                            labelText: "Enter Email",
                                            labelStyle: GoogleFonts.aBeeZee(),
                                            fillColor: Colors.white,
                                            border: new OutlineInputBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      12.0),
                                              borderSide: new BorderSide(),
                                            ),
                                            //fillColor: Colors.green
                                          ),
                                          validator: (val) {
                                            if (val.length == 0) {
                                              return "Email cannot be empty";
                                            } else {
                                              return null;
                                            }
                                          },
                                          keyboardType:
                                              TextInputType.emailAddress,
                                          style: new TextStyle(
                                            fontFamily: "Poppins",
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Text(
                                        "Password",
                                        style: GoogleFonts.acme(
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                50),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                4,
                                        child: TextFormField(
                                          controller:
                                              controller.password_controller,
                                          decoration: new InputDecoration(
                                            labelText: "Enter Password",
                                            fillColor: Colors.white,
                                            border: new OutlineInputBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      12.0),
                                              borderSide: new BorderSide(),
                                            ),
                                            //fillColor: Colors.green
                                          ),
                                          validator: (val) {
                                            if (val.length == 0) {
                                              return "Email cannot be empty";
                                            } else {
                                              return null;
                                            }
                                          },
                                          keyboardType:
                                              TextInputType.emailAddress,
                                          style: new TextStyle(
                                            fontFamily: "Poppins",
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 50,
                                      ),
                                      ElevatedButton(
                                        onPressed: () async {
                                          if (controller
                                                  .selectedresto.value.length >
                                              1) {
                                            controller.buttonPressFunc();
                                          } else {
                                            controller.isanimationactive.value =
                                                false;
                                            Get.snackbar("Error",
                                                "Please select a resturant",
                                                backgroundColor:
                                                    Colors.red[400],
                                                colorText: Colors.white);
                                          }
                                        },
                                        child: Container(
                                          height: 45,
                                          child: Center(child: Text('Sign in')),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Obx(
                    () => controller.isanimationactive.value
                        ? Stack(
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width,
                                height: 6,
                                color: Colors.grey[600],
                              ),
                              Transform.translate(
                                offset: Offset(
                                    MediaQuery.of(context).size.width *
                                        controller.animation.value,
                                    0),
                                child: Container(
                                  width: 175,
                                  height: 6,
                                  color: Colors.blue,
                                ),
                              ),
                            ],
                          )
                        : SizedBox(),
                  )
                ],
              ),
            ));
          }
        }),
      ),
    );
  }
}
