import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:resla_admin_v_1_0/app/modules/home/views/home_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthPageController extends GetxController
    with SingleGetTickerProviderMixin {
  //TODO: Implement AuthPageController
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  TextEditingController username_controller = TextEditingController();
  TextEditingController password_controller = TextEditingController();
  AnimationController animationController;
  Animation animation;
  final isanimationactive = false.obs;
  QuerySnapshot data;
  final isloading = true.obs;
  FirebaseAuth auth = FirebaseAuth.instance;
  final selectedresto = ''.obs;
  String test;
  void getdata() async {
    data = await firestore.collection('restaurants').get();
    isloading.value = false;
  }

  void buttonPressFunc() async {
    try {
      isanimationactive.value = true;
      animationController.repeat();
      UserCredential result = await auth.signInWithEmailAndPassword(
          email: username_controller.text, password: password_controller.text);
      final User user = result.user;
      QuerySnapshot userdata = await firestore
          .collection("restaurants")
          .where("owner", isEqualTo: user.uid)
          .get();
      SharedPreferences prefs = await SharedPreferences.getInstance();

      isanimationactive.value = false;

      userdata.docs.forEach((element) {
        if (element['name'] == selectedresto.value) {
          prefs.setString("document ID", element.id);
          prefs.setString("ResturantName", selectedresto.value);
          prefs.setString("email", user.email);
          prefs.setString("UID", user.uid);
          Get.toNamed('/home?docid=${element.id}');
        }
      });
    } on PlatformException catch (e) {
      isanimationactive.value = false;

      Get.snackbar("Error", e.message.toString());
    } on Exception catch (e) {
      isanimationactive.value = false;

      Get.snackbar("Error", e.toString());
    }
  }

  @override
  void onInit() {
    animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    animation =
        CurvedAnimation(parent: animationController, curve: Curves.linear);
    getdata();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    animationController.dispose();
  }
}
