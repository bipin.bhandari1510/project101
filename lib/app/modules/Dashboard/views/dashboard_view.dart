import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../controllers/dashboard_controller.dart';

class DashboardView extends GetView<DashboardController> {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  int level;
  String documentid;
  DashboardView({@required this.level, @required this.documentid});
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Positioned(
      top: 0.0,
      left: (level == 2 || level == 3) ? 75.0 : width * 0.18,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 375),
        height: MediaQuery.of(context).size.height,
        width: (level == 2)
            ? width
            : (level == 3)
                ? width
                : width,
        color: Color(0xffF1F6FA),
        child: Column(
          children: [
            SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 35.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 30.0,
                        ),
                        Text(
                          'Main Dashboard',
                          style: GoogleFonts.hammersmithOne(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: [
                SizedBox(
                  width: 45.0,
                ),
                ElevatedButton(
                  style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                          EdgeInsets.all(0)),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(12.0),
                      ))),
                  onPressed: () {},
                  child: Obx(
                    () => Container(
                      height: 130,
                      width: 250,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Color(0xFF42275a), Color(0xFF734b6d)],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 25,
                              width: 400,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  hint: Text(
                                    controller.selectedearningfilter.value,
                                    style: GoogleFonts.aBeeZee(
                                        fontSize: 13, color: Colors.white),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  items: <String>[
                                    'Today',
                                    'This Week',
                                    'This Month'
                                  ].map((String value) {
                                    return new DropdownMenuItem<String>(
                                      value: value,
                                      child: new Text(value),
                                    );
                                  }).toList(),
                                  onChanged: (_) {
                                    controller.selectedearningfilter.value = _;
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              'Total Earnings',
                              style: GoogleFonts.aBeeZee(fontSize: 21),
                            ),
                            controller.selectedearningfilter.value != "Today"
                                ? StreamBuilder(
                                    stream: firestore
                                        .collection('restaurants')
                                        .doc(documentid)
                                        .collection(
                                            '${DateTime.now().year}Orders')
                                        .doc('${DateTime.now().year}_1')
                                        .snapshots(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot snapshot) {
                                      if (snapshot.hasData) {
                                        return controller.selectedearningfilter
                                                    .value !=
                                                "This Week"
                                            ? Text(
                                                '\$ ${snapshot.data['total_earnings']}',
                                                style: GoogleFonts.aBeeZee(
                                                    fontSize: 24),
                                              )
                                            : Text(
                                                '\$ ${snapshot.data['week1totalearning']}',
                                                style: GoogleFonts.aBeeZee(
                                                    fontSize: 24),
                                              );
                                      } else {
                                        return Text(
                                          '-',
                                          style:
                                              GoogleFonts.aBeeZee(fontSize: 24),
                                        );
                                      }
                                    },
                                  )
                                : StreamBuilder(
                                    stream: firestore
                                        .collection('restaurants')
                                        .doc(documentid)
                                        .collection(
                                            '${DateTime.now().year}Orders')
                                        .doc('${DateTime.now().year}_1')
                                        .collection('Day${DateTime.now().day}')
                                        .where('status', isEqualTo: "Success")
                                        .snapshots(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot snapshot) {
                                      var totalearning = 0.0;
                                      if (snapshot.hasData) {
                                        snapshot.data.docs.forEach((e) {
                                          totalearning += e['price'];
                                        });
                                        return Text(
                                          '${totalearning}',
                                          style:
                                              GoogleFonts.aBeeZee(fontSize: 24),
                                        );
                                      } else {
                                        return Text(
                                          '-',
                                          style:
                                              GoogleFonts.aBeeZee(fontSize: 24),
                                        );
                                      }
                                    },
                                  ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 40,
                ),
                ElevatedButton(
                  style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                          EdgeInsets.all(0)),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(12.0),
                      ))),
                  onPressed: () {},
                  child: Obx(
                    () => Container(
                      height: 130,
                      width: 250,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Color(0xFF141e30), Color(0xFF243b55)],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 25,
                              width: 400,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  hint: Text(
                                    controller.selecteddeliveryfilter.value,
                                    style: GoogleFonts.aBeeZee(
                                        fontSize: 13, color: Colors.white),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  items: <String>[
                                    'Today',
                                    'This Week',
                                    'This Month'
                                  ].map((String value) {
                                    return new DropdownMenuItem<String>(
                                      value: value,
                                      child: new Text(value),
                                    );
                                  }).toList(),
                                  onChanged: (_) {
                                    controller.selecteddeliveryfilter.value = _;
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              'Completed Orders',
                              style: GoogleFonts.aBeeZee(fontSize: 21),
                            ),
                            controller.selecteddeliveryfilter.value != "Today"
                                ? StreamBuilder(
                                    stream: firestore
                                        .collection('restaurants')
                                        .doc(documentid)
                                        .collection(
                                            '${DateTime.now().year}Orders')
                                        .doc('${DateTime.now().year}_1')
                                        .snapshots(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot snapshot) {
                                      if (snapshot.hasData) {
                                        return controller.selecteddeliveryfilter
                                                    .value !=
                                                "This Week"
                                            ? Text(
                                                '${snapshot.data['completed_orders']}',
                                                style: GoogleFonts.aBeeZee(
                                                    fontSize: 24),
                                              )
                                            : Text(
                                                '${snapshot.data['week1totalorders']}',
                                                style: GoogleFonts.aBeeZee(
                                                    fontSize: 24),
                                              );
                                      } else {
                                        return Text(
                                          '-',
                                          style:
                                              GoogleFonts.aBeeZee(fontSize: 24),
                                        );
                                      }
                                    },
                                  )
                                : StreamBuilder(
                                    stream: firestore
                                        .collection('restaurants')
                                        .doc(documentid)
                                        .collection(
                                            '${DateTime.now().year}Orders')
                                        .doc('${DateTime.now().year}_1')
                                        .collection('Day${DateTime.now().day}')
                                        .where('status', isEqualTo: "Success")
                                        .snapshots(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot snapshot) {
                                      if (snapshot.hasData) {
                                        return Text(
                                          '${snapshot.data.docs.length}',
                                          style:
                                              GoogleFonts.aBeeZee(fontSize: 24),
                                        );
                                      } else {
                                        return Text(
                                          '-',
                                          style:
                                              GoogleFonts.aBeeZee(fontSize: 24),
                                        );
                                      }
                                    },
                                  ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 35,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(35, 0, 0, 0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Order Details',
                  style: GoogleFonts.hammersmithOne(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Obx(
              () => Row(
                children: [
                  SizedBox(
                    width: 45.0,
                  ),
                  Container(
                    child: GestureDetector(
                      onTap: () {
                        controller.istodayactive.value = true;
                      },
                      child: Text(
                        'Totay\'s order',
                        style: GoogleFonts.merriweatherSans(
                          color: controller.istodayactive.value
                              ? Color(0xff2F374C)
                              : Color(0xffBDC2D0),
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 40.0,
                  ),
                  Container(
                    child: GestureDetector(
                      onTap: () {
                        controller.istodayactive.value = false;
                      },
                      child: Text(
                        'Canceled Orders',
                        style: GoogleFonts.merriweatherSans(
                          color: controller.istodayactive.value == false
                              ? Color(0xff2F374C)
                              : Color(0xffBDC2D0),
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 8.0,
            ),
            Container(
              margin: EdgeInsets.only(left: 40.0, right: 40.0),
              height: 2.0,
              width: width,
              decoration: BoxDecoration(
                color: Color(0xffEDF3FA),
                borderRadius: BorderRadius.circular(1.0),
              ),
            ),
            Obx(
              () => StreamBuilder(
                  stream: controller.istodayactive.value
                      ? firestore
                          .collection('restaurants')
                          .doc(documentid)
                          .collection('${DateTime.now().year}Orders')
                          .doc('${DateTime.now().year}_1')
                          .collection('Day${DateTime.now().day}')
                          .orderBy('date')
                          .snapshots()
                      : firestore
                          .collection('restaurants')
                          .doc(documentid)
                          .collection('${DateTime.now().year}Orders')
                          .doc('${DateTime.now().year}_1')
                          .collection('Day${DateTime.now().day}')
                          .where('status', isEqualTo: "Canceled")
                          .snapshots(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    return Expanded(
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(30, 15, 30, 0),
                          child: Scrollbar(
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: Scrollbar(
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: DataTable(
                                    showCheckboxColumn: false,
                                    columns: [
                                      DataColumn(
                                          label: Text('SN',
                                              style: GoogleFonts.lato(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold)),
                                          tooltip:
                                              'represents serial number of the orders'),
                                      DataColumn(
                                          label: Text('Table No.',
                                              style: GoogleFonts.lato(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold)),
                                          tooltip:
                                              'represents the table number'),
                                      DataColumn(
                                          label: Text('Email',
                                              style: GoogleFonts.lato(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold)),
                                          tooltip:
                                              'represents the table number'),
                                      DataColumn(
                                          label: Text('Order Created',
                                              style: GoogleFonts.lato(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold)),
                                          tooltip:
                                              'represents the creation time of the order'),
                                      DataColumn(
                                          label: Text('Delivery Status',
                                              style: GoogleFonts.lato(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold)),
                                          tooltip:
                                              'represents the delivery status of the order'),
                                      DataColumn(
                                          label: Text('Total price',
                                              style: GoogleFonts.lato(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold)),
                                          tooltip:
                                              'represents the total price of the order'),
                                      DataColumn(
                                          label: Text('Discounts',
                                              style: GoogleFonts.lato(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold)),
                                          tooltip:
                                              'represents the total discount price of the order'),
                                      DataColumn(
                                          label: Text('Reward',
                                              style: GoogleFonts.lato(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold)),
                                          tooltip:
                                              'represents the total loyalty reward earned'),
                                      DataColumn(
                                          label: Text('Order Id',
                                              style: GoogleFonts.lato(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold)),
                                          tooltip:
                                              'represents the unique order id for each order'),
                                    ],
                                    rows: snapshot.hasData
                                        ? List.generate(
                                            snapshot.data.docs.length,
                                            (index) =>
                                                // we return a DataRow every time
                                                DataRow(
                                                    // List<DataCell> cells is required in every row
                                                    onSelectChanged: (val) {
                                                      List recepitdata = [];
                                                      snapshot
                                                          .data
                                                          .docs[index]
                                                              ['product']
                                                          .forEach((e) {
                                                        print(e
                                                            .toString()
                                                            .split('_')[0]);
                                                        recepitdata.add(
                                                          {
                                                            "Product": e
                                                                .toString()
                                                                .split('_')[0],
                                                            "Quantity": e
                                                                .toString()
                                                                .split('_')[1],
                                                            "Discount":
                                                                "\$ ${e.toString().split('_')[3]}",
                                                            "Price":
                                                                "\$ ${e.toString().split('_')[2]}",
                                                            "Method": snapshot
                                                                    .data
                                                                    .docs[index]
                                                                [
                                                                'paymentmethod']
                                                          },
                                                        );
                                                      });

                                                      showCupertinoModalBottomSheet(
                                                          context: context,
                                                          builder: (context) =>
                                                              //     ReceiptPage(
                                                              //   data: recepitdata,
                                                              //   totalprice: snapshot
                                                              //           .data
                                                              //           .docs[index]
                                                              //       ['price'],
                                                              // ),
                                                              Container());
                                                    },
                                                    cells: [
                                                      DataCell(Text(
                                                        '${index + 1}',
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        snapshot
                                                            .data
                                                            .docs[index]
                                                                ['tablenumber']
                                                            .toString(),
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        snapshot.data
                                                                .docs[index]
                                                            ['orderemail'],
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        "${snapshot.data.docs[index]['date'].toDate().toString().split(":")[0]}:${snapshot.data.docs[index]['date'].toDate().toString().split(":")[1]}"
                                                            .split(' ')[1],
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        snapshot.data
                                                                .docs[index]
                                                            ['status'],
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        snapshot
                                                            .data
                                                            .docs[index]
                                                                ['price']
                                                            .toString(),
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        snapshot.data
                                                                .docs[index]
                                                            ['discount'],
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        snapshot.data
                                                                .docs[index]
                                                            ['reward'],
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        "XCPSYYS",
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                    ])).toList()
                                        : List.generate(
                                            1,
                                            (index) =>
                                                // we return a DataRow every time
                                                DataRow(
                                                    // List<DataCell> cells is required in every row

                                                    cells: [
                                                      DataCell(Text(
                                                        '-',
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        '-',
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        '-',
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        "-",
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        '-',
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        '-',
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        '-',
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        '-',
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                      DataCell(Text(
                                                        "-",
                                                        style: GoogleFonts
                                                            .aBeeZee(),
                                                      )),
                                                    ])).toList(),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
