import 'package:get/get.dart';
import 'package:resla_admin_v_1_0/app/modules/Dashboard/controllers/dashboard_controller.dart';
import 'package:resla_admin_v_1_0/app/modules/Inventory/controllers/inventory_controller.dart';
import 'package:resla_admin_v_1_0/app/modules/Menu/controllers/menu_controller.dart';
import 'package:resla_admin_v_1_0/app/modules/StaffList/controllers/staff_list_controller.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<DashboardController>(
      () => DashboardController(),
    );
    Get.lazyPut<MenuController>(
      () => MenuController(),
    );
    Get.lazyPut<InventoryController>(
      () => InventoryController(),
    );
    Get.lazyPut<StaffListController>(
      () => StaffListController(),
    );
  }
}
