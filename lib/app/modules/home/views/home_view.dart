import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:resla_admin_v_1_0/app/modules/Dashboard/views/dashboard_view.dart';
import 'package:resla_admin_v_1_0/app/modules/FeedbackForm/views/feedback_form_view.dart';
import 'package:resla_admin_v_1_0/app/modules/Inventory/views/inventory_view.dart';
import 'package:resla_admin_v_1_0/app/modules/Menu/views/menu_view.dart';
import 'package:resla_admin_v_1_0/app/modules/SettingPage/views/setting_page_view.dart';
import 'package:resla_admin_v_1_0/app/modules/StaffList/views/staff_list_view.dart';
import 'package:resla_admin_v_1_0/app/widgets/SideBar.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    void select(int n) {
      for (int i = 0; i <= 5; i++) {
        if (i == n)
          isSelected[i] = true;
        else
          isSelected[i] = false;
      }

      controller.active.value = n;
    }

    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: LayoutBuilder(
          builder: (context, constraints) {
            print(constraints.minWidth);
            if (constraints.minWidth <= 1100.0 &&
                constraints.minWidth > 840.0) {
              return Stack(
                children: [
                  DashboardView(
                    level: 3,
                    documentid: Get.parameters['docid'],
                  ),
                  SideBar(
                    level: 2,
                  ),
                ],
              );
            }
            if (constraints.minWidth <= 840.0 && constraints.minWidth >= 0.0) {
              return Stack(
                children: [
                  DashboardView(level: 2, documentid: Get.parameters['docid']),
                  SideBar(
                    level: 2,
                  ),
                  Obx(
                    () => AnimatedPositioned(
                      duration: Duration(milliseconds: 375),
                      right: controller.toggle.value == 1 ? 250.0 : 0.0,
                      child: IconButton(
                        icon: Icon(
                          (controller.toggle.value == 0)
                              ? Icons.message
                              : Icons.cancel,
                          color: Colors.black,
                        ),
                        onPressed: () {
                          if (controller.toggle.value == 0)
                            controller.toggle.value = 1;
                          else
                            controller.toggle.value = 0;
                        },
                      ),
                    ),
                  )
                ],
              );
            } else
              return Obx(
                () => Stack(
                  children: [
                    controller.active.value == 0
                        ? DashboardView(
                            level: 5,
                            documentid: Get.parameters['docid'],
                          )
                        : controller.active.value == 1
                            ? MenuView()
                            : controller.active.value == 2
                                ? InventoryView()
                                : controller.active.value == 3
                                    ? FeedbackFormView()
                                    : controller.active.value == 4
                                        ? StaffListView()
                                        : SettingPageView(),
                    AnimatedContainer(
                      duration: Duration(milliseconds: 375),
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width * 0.18,
                      color: Colors.white,
                      child: Stack(
                        children: [
                          SingleChildScrollView(
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 40.0,
                                ),
                                Container(
                                  height: 40.0,
                                  width: 200.0,
                                  child: Row(
                                    children: [
                                      SizedBox(
                                        width: 20.0,
                                      ),
                                      Container(
                                        height: 50.0,
                                        width: 40.0,
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(8.0),
                                            child:
                                                Image.asset('assets/logo.png')),
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      AnimatedOpacity(
                                        opacity: 1.0,
                                        duration: Duration(milliseconds: 400),
                                        child: AnimatedContainer(
                                          duration: Duration(milliseconds: 375),
                                          width: 110.0,
                                          child: SingleChildScrollView(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                  'Yunmin Resturant',
                                                  style: GoogleFonts
                                                      .merriweatherSans(
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 30.0,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: navElements
                                      .map(
                                        (e) => NavElement(
                                          level: 1,
                                          index: navElements.indexOf(e),
                                          text: texts[navElements.indexOf(e)],
                                          icon: icons[navElements.indexOf(e)],
                                          active: isSelected[
                                              navElements.indexOf(e)],
                                          onTap: () {
                                            select(navElements.indexOf(e));
                                          },
                                        ),
                                      )
                                      .toList(),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: MediaQuery.of(context).size.height / 2 - 40.0,
                            right: 10.0,
                            child: Container(
                              height: 50.0,
                              width: 4.0,
                              decoration: BoxDecoration(
                                color: Colors.black12,
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
          },
        ),
      ),
    );
  }
}

class NavElement extends StatefulWidget {
  final bool active;
  final Function onTap;
  final IconData icon;
  final String text;
  final int index;
  final int level;

  NavElement(
      {this.level, this.onTap, this.active, this.icon, this.text, this.index});

  @override
  _NavElementState createState() => _NavElementState();
}

Color conColor = Colors.white;

class _NavElementState extends State<NavElement> with TickerProviderStateMixin {
  AnimationController _tcc;
  Animation<Color> _tca;
  AnimationController _icc;
  Animation<Color> _ica;
  double width = 140.0;
  double opacity = 0.0;

  @override
  void initState() {
    super.initState();
    _tcc = AnimationController(
        duration: Duration(milliseconds: 150),
        reverseDuration: Duration(milliseconds: 200),
        vsync: this);
    _tca = ColorTween(begin: Colors.black26, end: Colors.black54).animate(
      CurvedAnimation(
        parent: _tcc,
        curve: Curves.easeOut,
        reverseCurve: Curves.easeIn,
      ),
    );

    _tcc.addListener(() {
      setState(() {});
    });

    _icc = AnimationController(
        duration: Duration(milliseconds: 150),
        reverseDuration: Duration(milliseconds: 150),
        vsync: this);
    _ica = ColorTween(begin: Colors.blue, end: color).animate(
      CurvedAnimation(
        parent: _icc,
        curve: Curves.easeOut,
        reverseCurve: Curves.easeIn,
      ),
    );

    _icc.addListener(() {
      setState(() {});
    });

    if (widget.active) {
      _icc.forward();
      _tcc.forward();
    }

    Future.delayed(Duration(milliseconds: 150 * (widget.index + 1)), () {
      setState(() {
        width = 0.0;
        opacity = 1.0;
        //print(1000 ~/ (6 - (widget.index)));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!widget.active) {
      _icc.reverse();
    }
    return MouseRegion(
      onEnter: (value) {
        _tcc.forward();
      },
      onExit: (value) {
        _tcc.reverse();
      },
      opaque: false,
      child: GestureDetector(
        onTap: () {
          widget.onTap();
          _icc.forward();
          _tcc.forward();
        },
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15.0),
          ),
          padding: EdgeInsets.only(left: 30.0, top: 15.0, bottom: 15.0),
          height: 55.0,
          width: 200.0,
          child: Row(
            children: [
              AnimatedContainer(
                duration: Duration(milliseconds: 375),
                width: (widget.level == 2) ? 0.0 : 10.0,
                height: 0.0,
              ),
              Icon(
                widget.icon,
                color: widget.active ? _ica.value : _tca.value,
                size: 19.0,
              ),
              SizedBox(
                width: 8.0,
              ),
              Stack(
                children: [
                  AnimatedContainer(
                    duration: Duration(milliseconds: 375),
                    height: 60.0,
                    width: (widget.level == 2) ? 0.0 : 130.0,
                    alignment: Alignment((width == 0.0) ? -0.9 : -1.0,
                        (width == 0.0) ? 0.0 : -0.9),
                    child: AnimatedOpacity(
                      duration: Duration(milliseconds: 375),
                      opacity: opacity,
                      child: Text(
                        widget.text,
                        style: GoogleFonts.merriweatherSans(
                          fontWeight: FontWeight.bold,
                          color: widget.active ? _ica.value : _tca.value,
                          fontSize: 11.0,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 0.0,
                    child: AnimatedContainer(
                      height: 60.0,
                      width: width,
                      color: Colors.white,
                      duration: Duration(milliseconds: 375),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
