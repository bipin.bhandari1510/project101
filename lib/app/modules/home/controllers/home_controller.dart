import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:resla_admin_v_1_0/app/modules/home/views/home_view.dart';

class HomeController extends GetxController {
  List<NavElement> navElements = [
    NavElement(),
    NavElement(),
    NavElement(),
    NavElement(),
    NavElement(),
    NavElement(),
  ];
  List<String> texts = [
    'Dashboard',
    'Menu',
    'Inventory',
    'Feedback',
    'Staffs',
    'Settings',
  ];
  List<IconData> icons = [
    Icons.dashboard,
    Icons.menu,
    Icons.inventory,
    Icons.message,
    Icons.person_add,
    Icons.settings,
  ];
  List<bool> isSelected = [true, false, false, false, false, false];
  final active = 0.obs;
  final toggle = 0.obs;
  String restoid;
  @override
  void onInit() {
    restoid = Get.parameters['docid'];
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
