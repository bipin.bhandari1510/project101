import 'package:floating_action_bubble/floating_action_bubble.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:resla_admin_v_1_0/app/modules/StaffList/views/staff_details.dart';

import '../controllers/staff_list_controller.dart';

class StaffListView extends GetView<StaffListController> {
  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Save"),
      onPressed: () {},
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Staff Setting Configuration",
        style: GoogleFonts.acme(),
      ),
      content: Container(
        height: 250,
        width: 500,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Please check the features you want to enable to staff',
              style: GoogleFonts.lato(color: Colors.black54),
            ),
            SizedBox(
              height: 20,
            ),
            CheckboxListTile(
              title: Text("Allow cashier to edit menu items"),
              value: true,
              onChanged: (newValue) {},
              controlAffinity:
                  ListTileControlAffinity.leading, //  <-- leading Checkbox
            ),
            CheckboxListTile(
              title: Text("Allow cashier to preview feedback of users"),
              value: false,
              onChanged: (newValue) {},
              controlAffinity:
                  ListTileControlAffinity.leading, //  <-- leading Checkbox
            ),
            CheckboxListTile(
              title: Text("Allow to preview staffs details"),
              value: false,
              onChanged: (newValue) {},
              controlAffinity:
                  ListTileControlAffinity.leading, //  <-- leading Checkbox
            ),
          ],
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showAddStaff(BuildContext context) {
    showAnimatedDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Obx(
            () => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Add personal Details",
                  style: GoogleFonts.acme(),
                ),
                controller.emailerror.value.length > 0
                    ? SizedBox(
                        height: 10,
                      )
                    : SizedBox(),
                controller.emailerror.value.length > 0
                    ? Text(
                        controller.emailerror.value,
                        style: TextStyle(fontSize: 12, color: Colors.red),
                      )
                    : SizedBox(),
                Divider(),
              ],
            ),
          ),
          content: Container(
            width: 500,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  ' Full Name:',
                  style: GoogleFonts.abel(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: controller.nameController,
                  decoration: new InputDecoration(
                    labelText: "Enter name",
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(8.0),
                      borderSide: new BorderSide(),
                    ),
                    //fillColor: Colors.green
                  ),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Name cannot be empty";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                  style: new TextStyle(
                    fontFamily: "Poppins",
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  ' Email:',
                  style: GoogleFonts.abel(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: controller.emailController,
                  decoration: new InputDecoration(
                    labelText: "Enter Email",
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(8.0),
                      borderSide: new BorderSide(),
                    ),
                    //fillColor: Colors.green
                  ),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Email cannot be empty";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                  style: new TextStyle(
                    fontFamily: "Poppins",
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  ' Contact Number:',
                  style: GoogleFonts.abel(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: controller.contactController,
                  decoration: new InputDecoration(
                    labelText: "Enter contact number",
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(8.0),
                      borderSide: new BorderSide(),
                    ),
                    //fillColor: Colors.green
                  ),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Contact number cannot be empty";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                  style: new TextStyle(
                    fontFamily: "Poppins",
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  ' Address:',
                  style: GoogleFonts.abel(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                      child: TextFormField(
                        controller: controller.countryController,
                        decoration: new InputDecoration(
                          labelText: "Enter country",
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(8.0),
                            borderSide: new BorderSide(),
                          ),
                          //fillColor: Colors.green
                        ),
                        validator: (val) {
                          if (val.length == 0) {
                            return "Country number cannot be empty";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.emailAddress,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Expanded(
                      child: TextFormField(
                        controller: controller.cityController,
                        decoration: new InputDecoration(
                          labelText: "Enter City",
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(8.0),
                            borderSide: new BorderSide(),
                          ),
                          //fillColor: Colors.green
                        ),
                        validator: (val) {
                          if (val.length == 0) {
                            return "City name cannot be empty";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.emailAddress,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: controller.streetController,
                  decoration: new InputDecoration(
                    labelText: "Enter street name",
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(8.0),
                      borderSide: new BorderSide(),
                    ),
                    //fillColor: Colors.green
                  ),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Street name cannot be empty";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                  style: new TextStyle(
                    fontFamily: "Poppins",
                  ),
                ),
              ],
            ),
          ),
          actions: [
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                controller.nameController.clear();
                controller.emailController.clear();
                controller.contactController.clear();
                controller.cityController.clear();
                controller.countryController.clear();
                controller.streetController.clear();
                controller.emailerror.value = '';
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text("Next"),
              onPressed: () {
                if (controller.nameController.text.length > 0 &&
                    controller.emailController.text.length > 0 &&
                    controller.contactController.text.length > 0 &&
                    controller.cityController.text.length > 0 &&
                    controller.countryController.text.length > 0 &&
                    controller.streetController.text.length > 0) {
                  controller.emailerror.value = '';
                  Navigator.pop(context);
                  showAddStaffWorkDetails(context);
                } else {
                  controller.emailerror.value =
                      "Please make sure all fields are not empty";
                }
              },
            ),
          ],
        );
      },
      animationType: DialogTransitionType.slideFromLeft,
      curve: Curves.fastOutSlowIn,
      duration: Duration(seconds: 1),
    );
  }

  showAddStaffWorkDetails(BuildContext context) {
    showAnimatedDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Obx(
            () => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Add Work Details",
                  style: GoogleFonts.acme(),
                ),
                controller.error.value.length > 0
                    ? SizedBox(
                        height: 10,
                      )
                    : SizedBox(),
                controller.error.value.length > 0
                    ? Text(
                        controller.error.value,
                        style: TextStyle(fontSize: 12, color: Colors.red),
                      )
                    : SizedBox(),
                Divider(),
              ],
            ),
          ),
          content: Container(
            height: 400,
            width: 500,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  ' Department:',
                  style: GoogleFonts.abel(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 55,
                  width: 500,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black54),
                      borderRadius: BorderRadius.circular(8)),
                  child: DropdownButtonHideUnderline(
                    child: Obx(
                      () => DropdownButton<String>(
                        value: controller.department.value.length > 1
                            ? controller.department.value
                            : null,
                        hint: Text(
                          " Department",
                          style: GoogleFonts.aBeeZee(),
                          overflow: TextOverflow.ellipsis,
                        ),
                        items: <String>['Kitchen', 'Waiter', 'Cashier']
                            .map((String value) {
                          return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                        onChanged: (_) {
                          controller.department.value = _;
                        },
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  ' Shift:',
                  style: GoogleFonts.abel(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Container(
                      height: 55,
                      width: 200,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black54),
                          borderRadius: BorderRadius.circular(8)),
                      child: DropdownButtonHideUnderline(
                        child: Obx(
                          () => DropdownButton<String>(
                            value: controller.shiftfrom.value.length > 1
                                ? controller.shiftfrom.value
                                : null,
                            hint: Text(
                              "    From",
                              style: GoogleFonts.aBeeZee(),
                              overflow: TextOverflow.ellipsis,
                            ),
                            items: <String>[
                              '12:00 am',
                              '1:00 am',
                              '2:00 am',
                              '3:00 am',
                              '4:00 am',
                              '5:00 am',
                              '6:00 am',
                              '7:00 am',
                              '8:00 am',
                              '9:00 am',
                              '10:00 am',
                              '11:00 am',
                              '12:00 pm',
                              '1:00 pm',
                              '2:00 pm',
                              '3:00 pm',
                              '4:00 pm',
                              '5:00 pm',
                              '6:00 pm',
                              '7:00 pm',
                              '8:00 pm',
                              '9:00 pm',
                              '10:00 pm',
                              '11:00 pm'
                            ].map((String value) {
                              return new DropdownMenuItem<String>(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList(),
                            onChanged: (_) {
                              controller.shiftfrom.value = _;
                            },
                          ),
                        ),
                      ),
                    ),
                    Text('      -      '),
                    Container(
                      height: 55,
                      width: 200,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black54),
                          borderRadius: BorderRadius.circular(8)),
                      child: DropdownButtonHideUnderline(
                        child: Obx(
                          () => DropdownButton<String>(
                            value: controller.shiftto.value.length > 1
                                ? controller.shiftto.value
                                : null,
                            hint: Text(
                              "   To",
                              style: GoogleFonts.aBeeZee(),
                              overflow: TextOverflow.ellipsis,
                            ),
                            items: <String>[
                              '12:00 am',
                              '1:00 am',
                              '2:00 am',
                              '3:00 am',
                              '4:00 am',
                              '5:00 am',
                              '6:00 am',
                              '7:00 am',
                              '8:00 am',
                              '9:00 am',
                              '10:00 am',
                              '11:00 am',
                              '12:00 pm',
                              '1:00 pm',
                              '2:00 pm',
                              '3:00 pm',
                              '4:00 pm',
                              '5:00 pm',
                              '6:00 pm',
                              '7:00 pm',
                              '8:00 pm',
                              '9:00 pm',
                              '10:00 pm',
                              '11:00 pm'
                            ].map((String value) {
                              return new DropdownMenuItem<String>(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList(),
                            onChanged: (_) {
                              controller.shiftto.value = _;
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  ' Upload Photo and Verified Id:',
                  style: GoogleFonts.abel(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 20,
                ),
                Obx(
                  () => Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            height: 100,
                            width: 100,
                            color: Colors.grey[300],
                            child: controller.localimage.value.length == 0
                                ? Center(
                                    child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.upload),
                                      Text('Photo')
                                    ],
                                  ))
                                : Image(
                                    image: NetworkImage(
                                        controller.localimage.value),
                                    fit: BoxFit.fill,
                                  ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          controller.localimage.value.length == 0
                              ? Material(
                                  elevation: 2,
                                  color: Colors.orange,
                                  child: InkWell(
                                    onTap: () async {
                                      await controller.uploadToStorage();
                                    },
                                    child: Container(
                                      height: 35,
                                      width: 100,
                                      child: Center(
                                          child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "Select",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          controller.imageuploadindicator
                                                      .value ==
                                                  false
                                              ? SizedBox()
                                              : Container(
                                                  height: 15,
                                                  width: 15,
                                                  child:
                                                      CircularProgressIndicator(
                                                    strokeWidth: 4,
                                                  ),
                                                )
                                        ],
                                      )),
                                    ),
                                  ),
                                )
                              : Material(
                                  elevation: 2,
                                  color: Colors.red,
                                  child: InkWell(
                                    onTap: () async {
                                      await controller.removeImageFromDb();
                                    },
                                    child: Container(
                                      height: 35,
                                      width: 100,
                                      child: Center(
                                          child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "Remove",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          controller.imageuploadindicator
                                                      .value ==
                                                  false
                                              ? SizedBox()
                                              : Container(
                                                  height: 15,
                                                  width: 15,
                                                  child:
                                                      CircularProgressIndicator(
                                                    strokeWidth: 3,
                                                  ),
                                                )
                                        ],
                                      )),
                                    ),
                                  ),
                                ),
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            height: 100,
                            width: 100,
                            color: Colors.grey[300],
                            child: controller.idlocalimage.value.length == 0
                                ? Center(
                                    child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [Icon(Icons.upload), Text('ID')],
                                  ))
                                : Image(
                                    image: NetworkImage(
                                        controller.idlocalimage.value),
                                    fit: BoxFit.fill,
                                  ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          controller.idlocalimage.value.length == 0
                              ? Material(
                                  elevation: 2,
                                  color: Colors.orange,
                                  child: InkWell(
                                    onTap: () async {
                                      await controller.uploadIDToStorage();
                                    },
                                    child: Container(
                                      height: 35,
                                      width: 100,
                                      child: Center(
                                          child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "Select",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          controller.idimageuploadindicator
                                                      .value ==
                                                  false
                                              ? SizedBox()
                                              : Container(
                                                  height: 15,
                                                  width: 15,
                                                  child:
                                                      CircularProgressIndicator(
                                                    strokeWidth: 4,
                                                  ),
                                                )
                                        ],
                                      )),
                                    ),
                                  ),
                                )
                              : Material(
                                  elevation: 2,
                                  color: Colors.red,
                                  child: InkWell(
                                    onTap: () async {
                                      await controller.removeIDImageFromDb();
                                    },
                                    child: Container(
                                      height: 35,
                                      width: 100,
                                      child: Center(
                                          child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "Remove",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          controller.idimageuploadindicator
                                                      .value ==
                                                  false
                                              ? SizedBox()
                                              : Container(
                                                  height: 15,
                                                  width: 15,
                                                  child:
                                                      CircularProgressIndicator(
                                                    strokeWidth: 3,
                                                  ),
                                                )
                                        ],
                                      )),
                                    ),
                                  ),
                                ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          actions: [
            Obx(
              () => controller.uploadIndicator.value
                  ? SizedBox()
                  : FlatButton(
                      child: Text("Cancel"),
                      onPressed: () {
                        controller.nameController.clear();
                        controller.emailController.clear();
                        controller.contactController.clear();
                        controller.cityController.clear();
                        controller.countryController.clear();
                        controller.streetController.clear();
                        controller.emailerror.value = '';
                        controller.error.value = '';
                        controller.shiftfrom.value = '';
                        controller.shiftto.value = '';
                        controller.department.value = '';
                        Navigator.pop(context);
                      },
                    ),
            ),
            Obx(
              () => controller.uploadIndicator.value
                  ? CircularProgressIndicator()
                  : FlatButton(
                      child: Text("Finish"),
                      onPressed: () {
                        if (controller.shiftfrom.value.length > 0 &&
                            controller.shiftto.value.length > 0 &&
                            controller.department.value.length > 0 &&
                            controller.url.length > 1 &&
                            controller.idUrl.length > 1 &&
                            controller.imageuploadindicator.value == false) {
                          controller.uploadNewStaffToDB(context);
                        } else {
                          controller.error.value = "Please fill all the fields";
                        }
                      },
                    ),
            ),
          ],
        );
      },
      animationType: DialogTransitionType.slideFromRight,
      curve: Curves.fastOutSlowIn,
      duration: Duration(seconds: 1),
    );
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Positioned(
        left: width * 0.18,
        child: AnimatedContainer(
          duration: Duration(milliseconds: 375),
          height: MediaQuery.of(context).size.height,
          width: width / 1.22,
          color: Color(0xffF1F6FA),
          child: Scaffold(
            floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
            backgroundColor: Color(0xffF1F6FA),
            //Init Floating Action Bubble
            floatingActionButton: FloatingActionBubble(
              // Menu items
              items: <Bubble>[
                Bubble(
                  title: "Staff Settings",
                  iconColor: Colors.white,
                  bubbleColor: Colors.blue,
                  icon: Icons.settings,
                  titleStyle: TextStyle(fontSize: 16, color: Colors.white),
                  onPress: () {
                    controller.animationController.reverse();
                    showAlertDialog(context);
                  },
                ),
                // Floating action menu item
                Bubble(
                  title: "Add Staff",
                  iconColor: Colors.white,
                  bubbleColor: Colors.blue,
                  icon: Icons.person_add,
                  titleStyle: TextStyle(fontSize: 16, color: Colors.white),
                  onPress: () {
                    controller.animationController.reverse();
                    showAddStaff(context);
                  },
                ),
                // Floating action menu item

                //Floating action menu item
              ],

              // animation controller
              animation: controller.animation,

              // On pressed change animation state
              onPress: () => controller.animationController.isCompleted
                  ? controller.animationController.reverse()
                  : controller.animationController.forward(),

              // Floating Action button Icon color
              iconColor: Colors.white,

              // Flaoting Action button Icon
              iconData: Icons.add,
              backGroundColor: Colors.blue,
            ),
            body: Container(
              padding: EdgeInsets.all(40),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    "Staff List",
                    style: GoogleFonts.hammersmithOne(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    ),
                  ),
                  Divider(),
                  Expanded(
                    child: StreamBuilder(
                      stream: controller.firestore
                          .collection('restaurants')
                          .doc(controller.restoid)
                          .collection('staffs')
                          .snapshots(),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData) {
                          return GridView.builder(
                            itemCount: snapshot.data.docs.length,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount:
                                  MediaQuery.of(context).orientation ==
                                          Orientation.landscape
                                      ? 3
                                      : 2,
                              crossAxisSpacing: 8,
                              mainAxisSpacing: 8,
                              childAspectRatio: (3 / 1),
                            ),
                            itemBuilder: (
                              context,
                              index,
                            ) {
                              return ElevatedButton(
                                onPressed: () {
                                  showCupertinoModalBottomSheet(
                                      context: context,
                                      builder: (context) => StaffDetails(
                                            name: snapshot.data.docs[index]
                                                ['name'],
                                            email: snapshot.data.docs[index]
                                                ['email'],
                                            photo: snapshot.data.docs[index]
                                                ['image'],
                                            address:
                                                "${snapshot.data.docs[index]['country']}, ${snapshot.data.docs[index]['city']}",
                                            contactnumber: snapshot
                                                .data.docs[index]['contact'],
                                            department: snapshot
                                                .data.docs[index]['department'],
                                            id: snapshot.data.docs[index]
                                                ['docImage'],
                                            docId: snapshot.data.docs[index].id,
                                            joineddate: snapshot
                                                .data.docs[index]['joinedDate'],
                                            shift:
                                                "${snapshot.data.docs[index]['shiftfrom']}-${snapshot.data.docs[index]['shiftto']}",
                                          ));
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    children: [
                                      ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        child: Image.network(
                                          snapshot.data.docs[index]['image'],
                                          fit: BoxFit.fill,
                                          height: 150.0,
                                          width: 100.0,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Name: ${snapshot.data.docs[index]['name']}",
                                            style: GoogleFonts.lato(
                                                color: Colors.white),
                                          ),
                                          Text(
                                            "Department: ${snapshot.data.docs[index]['department']}",
                                            style: GoogleFonts.lato(
                                                color: Colors.white),
                                          ),
                                          Text(
                                            "ID: ${snapshot.data.docs[index]['email']}",
                                            style: GoogleFonts.lato(
                                                color: Colors.white),
                                          ),
                                          Text(
                                            "Shift: ${snapshot.data.docs[index]['shiftfrom']}-${snapshot.data.docs[index]['shiftto']}",
                                            style: GoogleFonts.lato(
                                                color: Colors.white),
                                          ),
                                          Text(
                                            "Contact: ${snapshot.data.docs[index]['contact']}",
                                            style: GoogleFonts.lato(
                                                color: Colors.white),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        } else {
                          return Center(child: CircularProgressIndicator());
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
