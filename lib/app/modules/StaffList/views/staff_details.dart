import 'package:date_format/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:interactiveviewer_gallery/hero_dialog_route.dart';
import 'package:resla_admin_v_1_0/app/modules/StaffList/controllers/staff_list_controller.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:table_calendar/table_calendar.dart';

class DemoSourceEntity {
  int id;
  String url;
  String previewUrl;
  String type;

  DemoSourceEntity(this.id, this.type, this.url, {this.previewUrl});
}

class StaffDetails extends GetView<StaffListController> {
  String name;
  String contactnumber;
  String email;
  String address;
  String photo;
  String id;
  String docId;
  String shift;
  String department;
  String joineddate;
  StaffDetails(
      {@required this.address,
      @required this.contactnumber,
      @required this.department,
      @required this.email,
      @required this.id,
      @required this.docId,
      @required this.name,
      @required this.photo,
      @required this.joineddate,
      @required this.shift});

  bool iseligible(String seletctedDate) {
    final currentTimeforEligiblity =
        formatDate(DateTime.now(), [d, '-', m, '-', yyyy]).toString();
    if (currentTimeforEligiblity == seletctedDate) {
      String ampmdetermination = shift.split('-')[1].split(' ')[1];
      int eligibilityhours;
      if (ampmdetermination == 'am') {
        eligibilityhours =
            int.parse(shift.split('-')[1].split(' ')[0].split(':')[0]);
      }
    } else {
      return false;
    }
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: controller.selectedTime,
    );
    if (picked != null) controller.selectedTime = picked;
    controller.hour = controller.selectedTime.hour.toString();
    controller.minute = controller.selectedTime.minute.toString();
    controller.time = controller.hour + ' : ' + controller.minute;
    controller.timeController.text = controller.time;
    controller.timeController.text = formatDate(
        DateTime(2019, 08, 1, controller.selectedTime.hour,
            controller.selectedTime.minute),
        [hh, ':', nn, " ", am]).toString();

    controller.dateTime.value = controller.timeController.text;
  }

  showAddToDo(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Assign"),
      onPressed: () async {
        if (controller.descriptionController.text.length > 0 &&
            controller.dateTime.value.length > 0) {
          String currentdate =
              formatDate(DateTime.now(), [h, ':', nn, " ", am]).toString();
          await controller.uploadToDotoDB(
              docId,
              controller.descriptionController.text,
              controller.dateTime.value,
              currentdate,
              controller.selectedDateForTodoFilter.value,
              "${DateTime.now().year}-${DateTime.now().month}-${DateTime.now().day}");
          Navigator.pop(context);
        }
      },
    );

    // set up the AlertDialogAlertDialog alert =

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Assign Todo",
            style: GoogleFonts.acme(),
          ),
          content: Container(
            width: 400,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: Colors.grey[700])),
                  child: TextField(
                    maxLines: 3,
                    controller: controller.descriptionController,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                        labelText: "Briefly describe task",
                        labelStyle: GoogleFonts.aBeeZee(),
                        floatingLabelBehavior: FloatingLabelBehavior.always),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                InkWell(
                  onTap: () {
                    _selectTime(context);
                  },
                  child: Container(
                    height: 55,
                    padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(color: Colors.grey[700])),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Obx(
                          () => Text(
                            controller.dateTime.value.length == 0
                                ? "Select Date"
                                : controller.dateTime.value,
                            style: TextStyle(color: Colors.grey[700]),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
          actions: [
            cancelButton,
            continueButton,
          ],
        );
      },
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Save"),
      onPressed: () {},
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Edit Details",
        style: GoogleFonts.acme(),
      ),
      content: Container(
        height: 300,
        width: 400,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: Colors.red)),
              child: TextField(
                decoration: InputDecoration(
                  border: InputBorder.none,
                  labelText: "Full Name",
                  labelStyle: GoogleFonts.aBeeZee(),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: Colors.red)),
              child: TextField(
                decoration: InputDecoration(
                  border: InputBorder.none,
                  labelText: "Address",
                  labelStyle: GoogleFonts.aBeeZee(),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: Colors.red)),
              child: TextField(
                decoration: InputDecoration(
                  border: InputBorder.none,
                  labelText: "Contact",
                  labelStyle: GoogleFonts.aBeeZee(),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 55,
              width: 400,
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.red),
                  borderRadius: BorderRadius.circular(2)),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  hint: Text(
                    " Department",
                    style: GoogleFonts.aBeeZee(),
                    overflow: TextOverflow.ellipsis,
                  ),
                  items: <String>['Kitchen', 'Waiter', 'Cashier']
                      .map((String value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList(),
                  onChanged: (_) {},
                ),
              ),
            ),
          ],
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(40),
        child: Row(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Personal Details",
                        style: GoogleFonts.acme(fontSize: 19),
                      ),
                      IconButton(
                          icon: Icon(CupertinoIcons.pencil_ellipsis_rectangle),
                          onPressed: () {
                            showAlertDialog(context);
                          })
                    ],
                  ),
                  Divider(),
                  Row(
                    children: [
                      Container(
                        color: Colors.blueGrey[100],
                        child: Image.network(
                          photo,
                          height: 150,
                          width: 100,
                          fit: BoxFit.fill,
                        ),
                      ),
                      SizedBox(
                        width: 25,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Name: ${name}",
                            style: controller.tstyle,
                          ),
                          SizedBox(
                            height: 7,
                          ),
                          Text(
                            "Address: ${address}",
                            style: controller.tstyle,
                          ),
                          SizedBox(
                            height: 7,
                          ),
                          Text(
                            "Contact: ${contactnumber}",
                            style: controller.tstyle,
                          ),
                          SizedBox(
                            height: 7,
                          ),
                          Text(
                            "Department: ${department}",
                            style: controller.tstyle,
                          ),
                          SizedBox(
                            height: 7,
                          ),
                          Text(
                            "Shift: ${shift}",
                            style: controller.tstyle,
                          )
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Text(
                    "Validated ID",
                    style: GoogleFonts.acme(fontSize: 19),
                  ),
                  Divider(),
                  Expanded(
                      child: Container(
                          color: Colors.blueGrey[100],
                          child: Hero(
                            tag: 1,
                            child: GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(
                                  HeroDialogRoute<void>(
                                    // DisplayGesture is just debug, please remove it when use
                                    builder: (BuildContext context) =>
                                        DemoImageItem(
                                            DemoSourceEntity(1, 'image', id)),
                                  ),
                                );
                              },
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Image.network(
                                    id,
                                    fit: BoxFit.cover,
                                    width: 300,
                                    height: 400,
                                  ),
                                ],
                              ),
                            ),
                          )))
                ],
              ),
            ),
            SizedBox(
              width: 50,
            ),
            Container(
              width: MediaQuery.of(context).size.width / 4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 12,
                  ),
                  Text(
                    "Attendance Records",
                    style: GoogleFonts.acme(fontSize: 19),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Divider(),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.blue[100],
                    ),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 8, 8, 1),
                          child: Text("Attendance stats of june 2021"),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Row(
                                children: [
                                  Text("Present: "),
                                  Text("21 days"),
                                ],
                              ),
                              Row(
                                children: [
                                  Text("Absent: "),
                                  Text("11 days"),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  StreamBuilder(
                    stream: controller.firestore
                        .collection('restaurants')
                        .doc(controller.restoid)
                        .collection('staffs')
                        .doc(docId)
                        .collection('attendance')
                        .snapshots(),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        controller.events.clear();
                        snapshot.data.docs.forEach((e) {
                          controller.events.addAll({
                            DateTime(
                                int.parse(e['year']),
                                int.parse(e['month']),
                                int.parse(e['day'])): [e['presence']]
                          });
                        });
                        return Container(
                          child: TableCalendar(
                            startDay: DateTime(
                                int.parse(joineddate.split('-')[0]),
                                int.parse(joineddate.split('-')[1]),
                                int.parse(joineddate.split('-')[2])),
                            calendarController: controller.calendarController,
                            startingDayOfWeek: StartingDayOfWeek.monday,
                            calendarStyle: CalendarStyle(
                              selectedColor: Colors.deepOrange[400],
                              todayColor: Colors.deepOrange[200],
                              markersColor: Colors.green[700],
                              markersAlignment: Alignment.bottomRight,
                              outsideDaysVisible: false,
                            ),
                            builders: CalendarBuilders(
                              markersBuilder:
                                  (context, date, events, holidays) {
                                final children = <Widget>[];
                                if (events[0] == true) {
                                  children.add(Container(
                                    height: 10,
                                    width: 10,
                                    decoration: BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                  ));
                                } else {
                                  children.add(Container(
                                    height: 10,
                                    width: 10,
                                    decoration: BoxDecoration(
                                      color: Colors.purple,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                  ));
                                }
                                ;
                                return children;
                              },
                            ),
                            headerStyle: HeaderStyle(
                              formatButtonTextStyle: TextStyle().copyWith(
                                  color: Colors.white, fontSize: 15.0),
                              formatButtonDecoration: BoxDecoration(
                                color: Colors.deepOrange[400],
                                borderRadius: BorderRadius.circular(16.0),
                              ),
                            ),
                            weekendDays: [7],
                            events: controller.events,
                            onDaySelected: controller.onDaySelected,
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Mark Attendance",
                    style: GoogleFonts.acme(fontSize: 19),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Divider(),
                  SizedBox(
                    height: 5,
                  ),
                  Material(
                    color: Color(0xff5663ff),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(16),
                      onTap: () {
                        Alert(
                          context: context,
                          type: AlertType.none,
                          title: "Mark Attendance?",
                          desc:
                              "Please press mark attendance if employee is present",
                          buttons: [
                            DialogButton(
                              child: Text(
                                "Mark Attendance",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                              onPressed: () {
                                String currentTime = formatDate(
                                        DateTime.now(), [hh, ':', nn, " ", am])
                                    .toString();
                              },
                            )
                          ],
                        ).show();
                      },
                      child: Container(
                        height: 55,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Mark Attendance",
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                  height: 1.5,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              width: 50,
            ),
            Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width / 3,
                  height: MediaQuery.of(context).size.height / 3.5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                        "Attendance Details",
                        style: GoogleFonts.acme(fontSize: 19),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Divider(),
                      Obx(
                        () => StreamBuilder(
                          stream: controller.firestore
                              .collection('restaurants')
                              .doc(controller.restoid)
                              .collection('staffs')
                              .doc(docId)
                              .collection('attendance')
                              .where('fulldate',
                                  isEqualTo: controller
                                      .selectedDateForTodoFilter.value)
                              .snapshots(),
                          builder: (BuildContext context, snapshot) {
                            if (snapshot.hasData) {
                              if (snapshot.data.docs.length == 0) {
                                return Container(
                                  width: 350,
                                  padding: EdgeInsets.all(10),
                                  color: Colors.orange[100],
                                  child: Center(
                                      child:
                                          Text("No record found for this day")),
                                );
                              } else {
                                if (snapshot.data.docs[0]['presence'] == true) {
                                  return Container(
                                    width: 350,
                                    padding: EdgeInsets.all(10),
                                    color: Colors.blue[100],
                                    child: Center(
                                        child: Text(
                                            "Attendance Recorded at ${snapshot.data.docs[0]['arrivalTime']}")),
                                  );
                                } else {
                                  return Container(
                                    width: 350,
                                    padding: EdgeInsets.all(10),
                                    color: Colors.red[100],
                                    child: Center(
                                        child:
                                            Text("User Has Asked For Leave")),
                                  );
                                }
                              }
                            } else {
                              return Center(child: CircularProgressIndicator());
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(),
                Container(
                  width: MediaQuery.of(context).size.width / 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 12,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "TO-DO TASK",
                            style: GoogleFonts.acme(fontSize: 19),
                          ),
                          Obx(() {
                            int year = int.parse(controller
                                .selectedDateForTodoFilter
                                .split('-')[2]);
                            int day = int.parse(controller
                                .selectedDateForTodoFilter
                                .split('-')[0]);
                            int month = int.parse(controller
                                .selectedDateForTodoFilter
                                .split('-')[1]);
                            if (DateTime.now().year <= year &&
                                DateTime.now().month <= month &&
                                DateTime.now().day <= day) {
                              return IconButton(
                                  icon: Icon(
                                      CupertinoIcons.pencil_ellipsis_rectangle),
                                  onPressed: () {
                                    showAddToDo(context);
                                  });
                            } else {
                              return SizedBox();
                            }
                          }),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Divider(),
                      Obx(
                        () => StreamBuilder(
                          stream: controller.firestore
                              .collection('restaurants')
                              .doc(controller.restoid)
                              .collection('staffs')
                              .doc(docId)
                              .collection('todos')
                              .where('day',
                                  isEqualTo: controller
                                      .selectedDateForTodoFilter.value)
                              .snapshots(),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            return snapshot.hasData
                                ? Container(
                                    width: 400,
                                    height: MediaQuery.of(context).size.height /
                                        2.2,
                                    child: ListView.builder(
                                      itemCount: snapshot.data.docs.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return Stack(
                                          children: [
                                            Container(
                                              padding: EdgeInsets.all(10),
                                              margin: EdgeInsets.all(10),
                                              color: Colors.blue[100],
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                      snapshot.data.docs[index]
                                                          ['task'],
                                                      style: TextStyle(
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight.w500)),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Row(
                                                    children: [
                                                      Text(
                                                        "Status:",
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      Text(
                                                        snapshot.data
                                                                .docs[index]
                                                            ['status'],
                                                        style: TextStyle(
                                                            color: Colors.red,
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Text(
                                                            "Assinged at: ",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 12,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                          Text(
                                                            "${snapshot.data.docs[index]['assigned day']} : ${snapshot.data.docs[index]['assigned time']}",
                                                            style: TextStyle(
                                                                color:
                                                                    Colors.red,
                                                                fontSize: 12,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ],
                                                      ),
                                                      Row(
                                                        children: [
                                                          Text(
                                                            "Deadline: ",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 12,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                          Text(
                                                            snapshot.data
                                                                    .docs[index]
                                                                ['deadline'],
                                                            style: TextStyle(
                                                                color:
                                                                    Colors.red,
                                                                fontSize: 12,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ],
                                                      )
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            snapshot.data.docs[index]
                                                        ['status'] ==
                                                    "incomplete"
                                                ? Align(
                                                    alignment:
                                                        Alignment.topRight,
                                                    child: InkWell(
                                                      onTap: () {
                                                        controller.deleteTODODB(
                                                            docId,
                                                            snapshot
                                                                .data
                                                                .docs[index]
                                                                .id);
                                                      },
                                                      child: Container(
                                                        margin:
                                                            EdgeInsets.fromLTRB(
                                                                4, 4, 4, 4),
                                                        height: 20,
                                                        width: 20,
                                                        decoration: BoxDecoration(
                                                            color:
                                                                Colors.red[400],
                                                            border: Border.all(
                                                                color: Colors
                                                                    .white),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10)),
                                                        child: Center(
                                                            child: Icon(
                                                          Icons.delete,
                                                          size: 12,
                                                          color:
                                                              Colors.grey[700],
                                                        )),
                                                      ),
                                                    ),
                                                  )
                                                : SizedBox(),
                                          ],
                                        );
                                      },
                                    ),
                                  )
                                : CircularProgressIndicator();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class DemoImageItem extends StatefulWidget {
  final DemoSourceEntity source;

  DemoImageItem(this.source);

  @override
  _DemoImageItemState createState() => _DemoImageItemState();
}

class _DemoImageItemState extends State<DemoImageItem> {
  @override
  void initState() {
    super.initState();
    print('initState: ${widget.source.id}');
  }

  @override
  void dispose() {
    super.dispose();
    print('dispose: ${widget.source.id}');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.of(context).pop(),
          ),
          Align(
            alignment: Alignment.center,
            child: Hero(
              tag: widget.source.id,
              child: Image.network(
                widget.source.url,
                fit: BoxFit.contain,
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width / 2,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
