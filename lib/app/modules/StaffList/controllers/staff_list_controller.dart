import 'dart:html';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:resla_admin_v_1_0/app/modules/home/controllers/home_controller.dart';
import 'package:table_calendar/table_calendar.dart';

class StaffListController extends GetxController
    with SingleGetTickerProviderMixin {
  Animation<double> animation;
  AnimationController animationController;
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController contactController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  RxString emailerror = ''.obs;
  RxString error = ''.obs;
  RxString department = ''.obs;
  RxString shiftfrom = ''.obs;
  RxString selectedDateForTodoFilter =
      '${DateTime.now().day}-${DateTime.now().month}-${DateTime.now().year}'
          .obs;
  RxString shiftto = ''.obs;
  RxString dateTime = ''.obs;
  RxBool imageuploadindicator = false.obs;
  RxBool idimageuploadindicator = false.obs;
  RxString localimage = ''.obs;
  RxString idlocalimage = ''.obs;
  RxBool uploadIndicator = false.obs;
  String restoid;
  String url;
  String idUrl;
  CalendarController calendarController;
  TextStyle tstyle = GoogleFonts.acme(fontSize: 15, height: 1.2);
  double height;
  double width;

  String setTime, setDate;

  String hour, minute, time;
  DateTime selectedDate = DateTime.now();

  TimeOfDay selectedTime = TimeOfDay(hour: 00, minute: 00);
  void onDaySelected(DateTime day, List events, List unknown) {
    selectedDateForTodoFilter.value =
        formatDate(day, [d, '-', m, '-', yyyy]).toString();
  }

  TextEditingController dateController = TextEditingController();
  TextEditingController timeController = TextEditingController();
  final Map<DateTime, List> events = {};
  final count = 0.obs;
  uploadNewStaffToDB(BuildContext context) async {
    await firestore
        .collection('restaurants')
        .doc(restoid)
        .collection('staffs')
        .doc()
        .set({
      "email": emailController.text,
      "image": url,
      "contact": contactController.text,
      "name": nameController.text,
      "city": cityController.text,
      "country": countryController.text,
      "street": streetController.text,
      "shiftfrom": shiftfrom.value,
      "shiftto": shiftto.value,
      "department": department.value,
      "docImage": idUrl
    });
    url = "";
    idUrl = '';
    nameController.clear();
    emailController.clear();
    contactController.clear();
    cityController.clear();
    countryController.clear();
    streetController.clear();
    emailerror.value = '';
    error.value = '';
    shiftfrom.value = '';
    shiftto.value = '';
    department.value = '';
    Navigator.pop(context);
  }

  uploadToDotoDB(String docid, String task, String deadline,
      String assignedTime, String day, String assignedday) async {
    await firestore
        .collection('restaurants')
        .doc(restoid)
        .collection('staffs')
        .doc(docid)
        .collection('todos')
        .doc()
        .set({
      "assigned time": assignedTime,
      "day": day,
      "deadline": deadline,
      "status": "incomplete",
      "task": task,
      "assigned day": assignedday
    });
    descriptionController.clear();
    dateTime.value = '';
  }

  deleteTODODB(String docid, String todoid) async {
    await firestore
        .collection('restaurants')
        .doc(restoid)
        .collection('staffs')
        .doc(docid)
        .collection('todos')
        .doc(todoid)
        .delete();
  }

  uploadIDToStorage() async {
    InputElement input = FileUploadInputElement()..accept = 'image/*';
    FirebaseStorage fs = FirebaseStorage.instance;
    input.click();
    input.onChange.listen((event) {
      final file = input.files.first;
      final reader = FileReader();
      reader.readAsDataUrl(file);
      print(file.relativePath);
      reader.onLoadEnd.listen((event) async {
        idimageuploadindicator.value = true;
        Random random = new Random();
        int randomNumber = random.nextInt(1000000);
        var snapshot =
            await fs.ref().child('my_image/$randomNumber').putBlob(file);
        String downloadUrl = await snapshot.ref.getDownloadURL();
        print(event.loaded);
        idUrl = downloadUrl;
        idlocalimage.value = downloadUrl;
        idimageuploadindicator.value = false;
      });
    });
  }

  uploadToStorage() async {
    InputElement input = FileUploadInputElement()..accept = 'image/*';
    FirebaseStorage fs = FirebaseStorage.instance;
    input.click();
    input.onChange.listen((event) {
      final file = input.files.first;
      final reader = FileReader();
      reader.readAsDataUrl(file);
      print(file.relativePath);
      reader.onLoadEnd.listen((event) async {
        imageuploadindicator.value = true;
        Random random = new Random();
        int randomNumber = random.nextInt(1000000);
        var snapshot =
            await fs.ref().child('my_image/$randomNumber').putBlob(file);
        String downloadUrl = await snapshot.ref.getDownloadURL();
        print(event.loaded);
        url = downloadUrl;
        localimage.value = downloadUrl;
        imageuploadindicator.value = false;
      });
    });
  }

  void removeImageFromDb() async {
    FirebaseStorage fs = FirebaseStorage.instance;
    Reference ref = await fs.refFromURL(localimage.value);
    await ref.delete();
    localimage.value = '';
  }

  void removeIDImageFromDb() async {
    FirebaseStorage fs = FirebaseStorage.instance;
    Reference ref = await fs.refFromURL(localimage.value);
    await ref.delete();
    idlocalimage.value = '';
  }

  @override
  void onInit() {
    calendarController = CalendarController();
    restoid = Get.find<HomeController>().restoid;
    animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 260),
    );

    final curvedAnimation =
        CurvedAnimation(curve: Curves.easeInOut, parent: animationController);
    animation = Tween<double>(begin: 0, end: 1).animate(curvedAnimation);

    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
