part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const AUTH_PAGE = _Paths.AUTH_PAGE;
  static const DASHBOARD = _Paths.DASHBOARD;
  static const MENU = _Paths.MENU;
  static const INVENTORY = _Paths.INVENTORY;
  static const FEEDBACK_FORM = _Paths.FEEDBACK_FORM;
  static const STAFF_LIST = _Paths.STAFF_LIST;
  static const SETTING_PAGE = _Paths.SETTING_PAGE;
}

abstract class _Paths {
  static const HOME = '/home';
  static const AUTH_PAGE = '/auth-page';
  static const DASHBOARD = '/dashboard';
  static const MENU = '/menu';
  static const INVENTORY = '/inventory';
  static const FEEDBACK_FORM = '/feedback-form';

  static const STAFF_LIST = '/staff-list';
  static const SETTING_PAGE = '/setting-page';
}
