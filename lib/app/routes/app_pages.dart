import 'package:get/get.dart';

import 'package:resla_admin_v_1_0/app/modules/AuthPage/bindings/auth_page_binding.dart';
import 'package:resla_admin_v_1_0/app/modules/AuthPage/views/auth_page_view.dart';
import 'package:resla_admin_v_1_0/app/modules/Dashboard/bindings/dashboard_binding.dart';
import 'package:resla_admin_v_1_0/app/modules/Dashboard/views/dashboard_view.dart';
import 'package:resla_admin_v_1_0/app/modules/FeedbackForm/bindings/feedback_form_binding.dart';
import 'package:resla_admin_v_1_0/app/modules/FeedbackForm/views/feedback_form_view.dart';
import 'package:resla_admin_v_1_0/app/modules/Inventory/bindings/inventory_binding.dart';
import 'package:resla_admin_v_1_0/app/modules/Inventory/views/inventory_view.dart';
import 'package:resla_admin_v_1_0/app/modules/Menu/bindings/menu_binding.dart';
import 'package:resla_admin_v_1_0/app/modules/Menu/views/menu_view.dart';
import 'package:resla_admin_v_1_0/app/modules/SettingPage/bindings/setting_page_binding.dart';
import 'package:resla_admin_v_1_0/app/modules/SettingPage/views/setting_page_view.dart';
import 'package:resla_admin_v_1_0/app/modules/StaffList/bindings/staff_list_binding.dart';
import 'package:resla_admin_v_1_0/app/modules/StaffList/views/staff_list_view.dart';
import 'package:resla_admin_v_1_0/app/modules/home/bindings/home_binding.dart';
import 'package:resla_admin_v_1_0/app/modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.AUTH_PAGE;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.AUTH_PAGE,
      page: () => AuthPageView(),
      binding: AuthPageBinding(),
    ),
    GetPage(
      name: _Paths.DASHBOARD,
      page: () => DashboardView(),
      binding: DashboardBinding(),
    ),
    GetPage(
      name: _Paths.MENU,
      page: () => MenuView(),
      binding: MenuBinding(),
    ),
    GetPage(
      name: _Paths.INVENTORY,
      page: () => InventoryView(),
      binding: InventoryBinding(),
    ),
    GetPage(
      name: _Paths.FEEDBACK_FORM,
      page: () => FeedbackFormView(),
      binding: FeedbackFormBinding(),
    ),
    GetPage(
      name: _Paths.STAFF_LIST,
      page: () => StaffListView(),
      binding: StaffListBinding(),
    ),
    GetPage(
      name: _Paths.SETTING_PAGE,
      page: () => SettingPageView(),
      binding: SettingPageBinding(),
    ),
  ];
}
